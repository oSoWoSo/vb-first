#!/usr/bin/bash

#
# git push everywhere linux
#
# Copyright (c) 2022 zenobit from oSoWoSo
# mail: <pm@osowoso.xyz> web: https://osowoso.xyz
# licensed under EUPL 1.2
# source:
#

## COLORS
c0=$(tput setaf 0) # black
c1=$(tput setaf 1) # red
c2=$(tput setaf 2) # green
c3=$(tput setaf 3) # yellow
c4=$(tput setaf 4) # blue
c5=$(tput setaf 5) # 
c6=$(tput setaf 6) # light blue
c7=$(tput setaf 7) # white
c8=$(tput setaf 8) # grey
c9=$(tput setaf 9) # 
c=$(tput sgr0)

## ensure you are in git repo
FILE='./.git'
if [ -f "$FILE" ]; then
    echo "$FILE exists..."
else 
    echo "${c1}You are not in git repository! Exiting${c}"
    exit 255
fi

REPO=$(pwd | rev |cut -d'/' -f 1 | rev)
#read -p "Where will be your main repository?" host
#echo "${c2}pushed to ${c}"

# set default
git branch --set-upstream-to=pi/main main
git push --set-upstream pi main
echo "default setted"
# pi
git remote add pi git@10.0.1.3:oSoWoSo/"$REPO".git
git push pi main
echo "${c2}pushed to pi${c}"
# github
git remote add github git@github.com:oSoWoSo/"$REPO".git
git push github main
echo "${c2}pushed to github${c}"
# codeberg
git remote add codeberg git@codeberg.org:oSoWoSo/"$REPO".git
git push codeberg main
echo "${c2}pushed to codeberg${c}"
# disroot
git remote add disroot git@git.disroot.org:oSoWoSo/"$REPO".git
git push disroot main
echo "${c2}pushed to disroot${c}"
# sourceforge

# gitlab

# sourcehut

# 


