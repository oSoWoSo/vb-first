#!/usr/bin/bash

#
# chroot install script for vb aka VoidGNU/Linux
#
# Copyright (c) 2022 zenobit from oSoWoSo
# mail: <zen@osowoso.xyz> web: https://osowoso.xyz
# licensed under EUPL 1.2
# source:
# 

## COLORS
c0=$(tput setaf 0) # black
c1=$(tput setaf 1) # red
c2=$(tput setaf 2) # green
c3=$(tput setaf 3) # yellow
c4=$(tput setaf 4) # blue
c5=$(tput setaf 5) # 
c6=$(tput setaf 6) # light blue
c7=$(tput setaf 7) # white
c8=$(tput setaf 8) # grey
c9=$(tput setaf 9) # 
# reset color
n=$(tput sgr0)
# background
b0=$(tput setab 0)
b1=$(tput setab 1)
b2=$(tput setab 2)
b3=$(tput setab 3)
b4=$(tput setab 4)
b5=$(tput setab 5)
b6=$(tput setab 6)
b7=$(tput setab 7)
b8=$(tput setab 8)
b9=$(tput setab 9)

echo -e "\n${c2}Running part 2...${n}\n"
echo -e "\n${c2}Changing mirrors to CZ one${n}\n"
mkdir -p /etc/xbps.d
cp /usr/share/xbps.d/*-repository-*.conf /etc/xbps.d/
sed -i 's|https://alpha.de.repo.voidlinux.org|https://mirror.fit.cvut.cz/voidlinux|g' /etc/xbps.d/*-repository-*.conf

echo -e "\n${c2}After changing the URLs, you must synchronize xbps with the new mirrors:${n}\n"
xbps-install -S
xbps-install -Suvy xbps
xbps-install -Suvy

echo -e "\n${c2}Install base-system etc. OR${n}\n"
xbps-install -y base-system ngetty curl #connman

echo -e "\n${c2}Install base-minimal etc.${n}\n"
#xbps-install -y base-minimal ngetty glibc-locales bash gzip file sed gawk less util-linux which tar xz man-pages \
#            mdocml shadow e2fsprogs btrfs-progs xfsprogs f2fs-tools dosfstools procps-ng tzdata pciutils usbutils \
#            iana-etc openssh kbd iproute2 iputils iw xbps nvi sudo void-artwork traceroute ethtool kmod acpid \
#            eudev runit-void removed-packages
xbps-remove -y base-voidstrap

echo -e "\n${c2}Add services${n}\n"
rm -vf /var/service/agetty* && ln -s /etc/sv/ngetty /var/service/
ln -s /etc/sv/dbus /var/service/
#ln -s /etc/sv/connmand /var/service/
ln -s /etc/sv/NetworkManager /var/service/
ln -s /etc/sv/sshd /var/service/

echo -e "\n${c2}Enter your hostname...${n}\n"
read hostname
echo $hostname > /etc/hostname

# Go through the options in
#nano /etc/rc.conf

echo -e "\n${c2}Change timezone${n}\n"
ln -sf /usr/share/zoneinfo/Europe/Prague /etc/localtime

# Change Glibc locales if you want (Not needed for English)
#nano /etc/default/libc-locales
#xbps-reconfigure -f glibc-locales

echo -e "\n${c2}Change fstab${n}\n"
tee -a /etc/fstab > /dev/null <<EOF
/dev/${root} / btrfs defaults,noatime 0 1
/dev/${boot} /boot vfat defaults,noatime 0 2
/dev/${home} /home btrfs defaults,noatime 0 2
/dev/${cache} /var/cache btrfs defaults,noatime 0 2
tmpfs /tmp tmpfs defaults,nosuid,nodev 0 0
EOF

echo -e "\n${c2}Install grub or${n}\n"
xbps-install -y grub grub-x86_64-efi refind
grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id="Void"
echo -e "\n${c2}Install refind instead (EFI only)${n}\n"
refind-install

# Add new user
echo -e "\n${c2}Add password to root${n}\n"
passwd

groupadd $USER _ulimit && sed -ie 48'a \@_ulimit\thard\tnofile\t524288' /etc/security/limits.conf

echo -e "\n${c2}Enter your new username...${n}\n"
read user
useradd -mG audio,cdrom,disk,floppy,kvm,mail,network,optical,scanner,storage,video,wheel,_ulimit $user
passwd $user

echo -e "\n${c2}Finalization${n}\n"
xbps-reconfigure -afv

echo -e "\n${c2}Downloading third part...${n}\n"
curl http://10.0.1.3:3000/oSoWoSo/vb-scripts/raw/branch/master/vb3.sh -o /home/$user/vb3.sh
chmod +x /home/$user/vb3.sh
echo -e "\n${c2}Now you can exit chroot and reboot${n}"
echo -e "${c2}After reboot run ./vb3.sh${n}\n"
