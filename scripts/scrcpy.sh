#!/usr/bin/bash

#
# scrcpy install script for vb aka VoidGNU/Linux
#
# Copyright (c) 2021 oSoWoSo <zen@osowoso.xyz>
# https://oSoWoSo.xyz
# licensed under EUPL 1.2
# sources:
#

sudo xbps-install\
 scrcpy

echo done
