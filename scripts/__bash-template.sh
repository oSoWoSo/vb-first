#!/usr/bin/bash
#  install script for void vb linux
# Copyright (c) 2021 oSoWoSo <zen@osowoso.xyz>
# https://oSoWoSo.xyz
# licensed under EUPL 1.2
# sources:
# ***CHANGME***

## COLORS
c0=$(tput setaf 0) # black
c1=$(tput setaf 1) # red
c2=$(tput setaf 2) # green
c3=$(tput setaf 3) # yellow
c4=$(tput setaf 4) # blue
c5=$(tput setaf 5) # 
c6=$(tput setaf 6) # light blue
c7=$(tput setaf 7) # white
c8=$(tput setaf 8) # grey
c9=$(tput setaf 9) # 
# reset color
n=$(tput sgr0)
#bn=$(tput sgr0)
# background
b0=$(tput setab 0)
b1=$(tput setab 1)
b2=$(tput setab 2)
b3=$(tput setab 3)
b4=$(tput setab 4)
b5=$(tput setab 5)
b6=$(tput setab 6)
b7=$(tput setab 7)
b8=$(tput setab 8)
b9=$(tput setab 9)
## COLOR TEST
test_colors () {
	echo Color test:
	echo ${c0}${b7}black${n} ${b1}on red${n}
	echo ${c1}red ${b2}on green${n}
	echo ${c2}green ${b3}on yellow${n}
	echo ${c3}yellow ${b4}on blue${n}
	echo ${c4}blue ${b5}on nevim${n}
	echo ${c5}nevim ${b6}on light blue${n}
	echo ${c6}nevim ${b7}on white${n}
	echo ${c7}white ${b8}on grey${n}
	echo ${c8}grey ${b9}on nevim${n}
	echo ${c9}nevim ${b0}on black${n}
}
test_colors
## QUESTION AND ANSWER
printf "\n[1;4mPlease input [0;1m:[0m\n"
printf "(Please keep UNIX-compliant): "
read MY_VARIABLE
## ROOTCHECK
check_root () {
	if [[ $EUID -gt 0 ]]; then
		echo -e "\n${c1}This operation needs super-user privileges."
		echo -e "${c2}exiting...${n}"
		exit 255
	fi
}
check_root
## USERCHECK
echo "Your user is:"
user=$USER
echo -n "$r"
echo $user
echo -n "$n"
echo
## CASE QUESTION
echo -n "$g"
echo "choices:"
echo -n "$n"
echo "0 - ***changme***"
echo "1 - ***changme***"
echo "2 - ***changme***"
echo "3 - ***changme***"
echo "4 - ***changme***"
echo "5 - ***changme***"
echo "q - quit"
echo -n "$g"
read -p "posible answers: (y/N) " ***changme***
echo -n "$n"
case $***changme*** in
	y )
	***changme***
	echo -n "$g"
	echo "Done"
	echo -n "$n"
	;;

	n )
	;;
esac
### ANOTHER CASE
	5 )
	echo -n "$g"
	echo ***changme***
	echo -n "$n"
	***changme***
	echo -n "$g"
	echo "Done"
	echo -n "$n"
	;;
### NOT HERE YET
echo "***changme***"
### BLUE 
echo -n "$b"
echo "***changme***"
echo -n "$n"
### GREEN 
echo -n "$g"
echo "***changme***"
echo -n "$n"
### RED 
echo -n "$r"
echo "***changme***"
echo -n "$n"
### DONE
echo -n "$g"
echo "Done"
echo -n "$n"
