#!/usr/bin/bash

#
# chezmoi install script for vb aka VoidGNU/Linux
#
# Copyright (c) 2022 zenobit from oSoWoSo
# mail: <zen@osowoso.xyz> web: https://osowoso.xyz
# licensed under EUPL 1.2
# source:
# 

sudo xbps-install\
 chezmoi

chezmoi init
chezmoi cd
git init
git add .

echo done
