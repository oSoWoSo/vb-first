#!/usr/bin/bash

#
# onedev install script for vb aka VoidGNU/Linux
#
# Copyright (c) 2022 zenobit from oSoWoSo
# mail: <zen@osowoso.xyz> web: https://osowoso.xyz
# licensed under EUPL 1.2
# source:
# 

cd
mkdir $user-bin
cd $user-bin
sudo xbps-install\
 curl\
 git\
 openjdk-jre\
 unzip

sudo groupadd $USER ulimit524288 &&
sudo sed -ie 48'a \@ulimit524288\thard\tnofile\t524288' /etc/security/limits.conf

# Look for current version at: https://code.onedev.io/projects/160/builds?query=%22Job%22+is+%22Release%22
curl https://code.onedev.io/downloads/projects/160/builds/2222/artifacts/onedev-6.2.2.zip -o onedev.zip
unzip onedev.zip && rm onedev.zip
cd onedev-6.0.0/bin
./server.sh console
