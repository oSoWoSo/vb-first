#!/bin/bash

#
# void final install script for vb aka VoidGNU/Linux
#
# Copyright (c) 2022 zenobit from oSoWoSo
# mail: <zen@osowoso.xyz> web: https://osowoso.xyz
# licensed under EUPL 1.2
# sources: https://git.ipv6.sk/Zezik/void/src/branch/master/void.sh
#

## COLORS
c0=$(tput setaf 0) # black
c1=$(tput setaf 1) # red
c2=$(tput setaf 2) # green
c3=$(tput setaf 3) # yellow
c4=$(tput setaf 4) # blue
c5=$(tput setaf 5) # 
c6=$(tput setaf 6) # light blue
c7=$(tput setaf 7) # white
c8=$(tput setaf 8) # grey
c9=$(tput setaf 9) # 
# reset color
n=$(tput sgr0)
# background
b0=$(tput setab 0)
b1=$(tput setab 1)
b2=$(tput setab 2)
b3=$(tput setab 3)
b4=$(tput setab 4)
b5=$(tput setab 5)
b6=$(tput setab 6)
b7=$(tput setab 7)
b8=$(tput setab 8)
b9=$(tput setab 9)

version="0.6.2"
progname=${0##*/}

echo "${c0}${b7}# $progname - Version: $version                #"
echo "${c0}${b7}# Original author: Raven                        #"
echo "${c1}${b0}# SRC: https://codeberg.org/jhroot/void-sh   #"
echo "${c1}${b0}# This fork: oSoWoSo <osowoso@protonmail.com>  #"
echo "${c3}${b0}# https://codeberg.org/oSoWoSo                  #"
echo "${c3}${b0}# XBPS version: $(xbps-query -v --version | sed 's/GIT: UNSET//')     #"
echo
echo -e "\n${c1}Let's begin installing...${n}\n"

echo -e "\n${c2}Choose answer or press Enter for next question...${n}\n"

echo -e "\n${c4}Adding dbus and connmand service...${n}\n"
sudo ln -s /etc/sv/dbus /var/service/
sudo ln -s /etc/sv/dhcpcd /var/service/
echo -e "\n${c2}Done${n}\n"

# Update the System

echo -e "\n${c4}Checking for updates...${n}\n"
sudo xbps-install -Su
echo -e "\n${c2}Done${n}\n"

# Install recommended packages

echo -e "\n${c4}Install recommended packages...${n}\n"
sudo xbps-install curl wget unzip zip gptfdisk mtools mlocate \
	ntfs-3g fuse-exfat bash-completion linux-headers dkms
echo -e "\n${c2}Done${n}\n"

# Install development packages

read -p "Do you want to install development tools? (y) " devtools
case $devtools in
	y )
	sudo xbps-install autoconf automake bison m4 make libtool flex meson ninja \
		optipng sassc
	echo -e "\n${c2}Done${n}\n"
	;;

esac

# Install Non free and multilib repository

read -p "Do you want to install non-free and multilib repository? (y) " nonfree
case $nonfree in
	y )
	sudo xbps-install void-repo-nonfree void-repo-multilib void-repo-multilib-nonfree
	sudo xbps-install -S
	echo -e "\n${c2}Done${n}\n"
# Install Latest Nvidia proprietary drivers
	read -p "Do you want to install latest Nvidia proprietary drivers? (y) " nvidia
	case $nvidia in
		y )
		sudo xbps-remove fx86-video-nouveau
		sudo xbps-install nvidia
		echo -e "\n${c2}Done${n}\n"
		;;

	esac
	;;

esac

# Install shell

echo -e "\n${c4}Change user default shell...(Enter for leaving default bash)${n}\n"
printf "Possible (type in number): \n
- 1 Fish
- 2 Zsh\n"
read -p "Which shell do you want?" shell
case $shell in
	1 )
	echo -e "\n${c4}Install Fish...${n}\n"
	sudo xbps-install\
	fish-shell
	
	sudo usermod --shell /bin/fish "$USER"
	;;

	2 )
	echo -e "\n${c4}Install Zsh...${n}\n"
	sudo xbps-install\
	zsh\
	zsh-autosuggestions\
	zsh-completions\
	zsh-history-substring-search\
	zsh-syntax-highlighting
	
	sudo usermod --shell /bin/zsh "$USER"
	;;

esac

# Graphical User interface

echo -e "\n${c4}Install Graphical user interface?${n}\n"
printf "Possible (type in number): \n
- 1 Desktop environment
- 2 Window manager
- 3 Xorg only
- 4 Wayland Only\n"
read -p "Do you want to instal Desktop environment or Window manager?" gui
case $gui in
	1 )
	echo -e "\n${c4}Which Desktop environment do you want?${n}\n"
	printf "Possible (type in number): \n
	- 1 Xfce
	- 2 MATE
	- 3 GNOME
	- 4 KDE Plasma
	- 5 Budgie
	- 6 Cinnamon
	- 7 LXQt
	- 8 Enlightenment
	- 9 LXDE
	- 10 Lumina\n"
	read -p "Which Desktop environment do you want?" desktop
	case $desktop in
		1 )
# Xfce
		echo -e "\n${c4}Install Xfce...${n}\n"
		sudo xbps-install xorg-minimal xfce4-appfinder xfce4-battery-plugin xfce4-clipman-plugin xfce4-cpufreq-plugin \
			xfce4-cpugraph-plugin xfce4-dict xfce4-diskperf-plugin xfce4-fsguard-plugin \
			xfce4-genmon-plugin xfce4-mailwatch-plugin xfce4-mpc-plugin xfce4-netload-plugin \
			xfce4-notifyd xfce4-panel xfce4-panel-appmenu xfce4-places-plugin xfce4-power-manager \
			xfce4-pulseaudio-plugin xfce4-screensaver xfce4-screenshooter xfce4-sensors-plugin \
			xfce4-session xfce4-settings xfce4-systemload-plugin xfce4-taskmanager xfce4-terminal \
			xfce4-timer-plugin xfce4-verve-plugin xfce4-whiskermenu-plugin xfce4-xkb-plugin \
			Thunar thunar-volman thunar-archive-plugin thunar-media-tags-plugin ristretto \
			xarchiver mousepad xfwm4 xfdesktop zathura zathura-pdf-poppler gvfs gvfs-mtp gvfs-gphoto2 \
			xfce-polkit parole
		echo -e "\n${c2}Done${n}\n"
		;;

		2 )
# Mate
		echo -e "\n${c4}Install MATE...${n}\n"
		sudo xbps-install xorg-minimal mate-applets mate-backgrounds mate-calc mate-control-center mate-desktop \
			mate-icon-theme mate-indicator-applet mate-media mate-menus mate-notification-daemon \
			mate-panel mate-panel-appmenu mate-screensaver mate-sensors-applet mate-session-manager \
			mate-settings-daemon mate-system-monitor mate-terminal mate-themes mate-tweak mate-utils \
			mozo pluma caja caja-image-converter caja-sendto caja-open-terminal caja-wallpaper \
			caja-xattr-tags eom atril gvfs gvfs-mtp gvfs-gphoto2 engrampa mate-power-manager mate-polkit
		echo -e "\n${c2}Done${n}\n"
		;;

		3 )
# Gnome
		echo -e "\n${c4}Install GNOME...${n}\n"
		sudo xbps-install xorg-minimal gnome-shell gnome-control-center gnome-tweaks gnome-system-monitor gnome-terminal gdm \
			gnome-disk-utility nautilus nautilus-sendto gvfs gvfs-mtp gvfs-gphoto2 eog eog-plugins \
			evince gedit gedit-plugins gnome-video-effects gnome-themes-extra gnome-session gnome-screenshot \
			gnome-shell-extensions gnome-icon-theme gnome-icon-theme-extras gnome-icon-theme-symbolic \
			gnome-backgrounds file-roller chrome-gnome-shell totem
		echo -e "\n${c4}Includes: GNOME Calendar, GNOME Clocks, GNOME Weather, Evolution, GNOME Font Viewer,"
		echo "GNOME Calculator, GNOME Characters, GNOME Contacts, GNOME Documents, GNOME Maps${n}\n"
		read -p "Do you want to install GNOME applications? (y) " gnomeapps
		case $gnomeapps in
			y )
			echo -e "\n${c4}Install GNOME applications...${n}\n"
			sudo xbps-install gnome-calendar gnome-clocks gnome-weather evolution gnome-font-viewer \
				gnome-calculator gnome-characters gnome-contacts gnome-documents gnome-maps
			;;

		esac

		echo -e "\n${c2}Done${n}\n"
		;;

		4 )
# KDE Plasma
		echo -e "\n${c4}Install KDE Plasma...${n}\n"
		sudo xbps-install xorg-minimal plasma-desktop plasma-disks plasma-thunderbolt plasma-systemmonitor plasma-pa plasma-nm \
			plasma-firewall plasma-browser-integration plasma-vault latte-dock oxygen kdegraphics-thumbnailers \
			dolphin dolphin-plugins kate5 konsole okular gwenview ark sddm sddm-kcm yakuake spectacle \
			partitionmanager ffmpegthumbs kde-gtk-config5
# KDE Apps
		echo -e "\n${c4}Includes: KMail, Kontact, KOrganizer, KAddressbook, Akregator, Konversation, KCalc, KCharSelect${n}\n"
		read -p "Do you want to install KDE applications? (y) " kdeapps
		case $kdeapps in
			y )
			echo -e "\n${c4}Install KDE applications...${n}\n"
			sudo xbps-install kmail kontact korganizer kaddressbook akregator konversation kcalc kcharselect
			;;

		esac

# KDE Connect
		read -p "Do you want to use KDE Connect? (y) " kdeconnect
		case $kdeconnect in
			y )
			echo -e "\n${c4}Install KDE Connect...${n}\n"
			sudo xbps-install kdeconnect
			;;

		esac

		echo -e "\n${c2}Done${n}\n"
		;;

		5 )
# Budgie
		echo -e "\n${c4}Install Budgie...${n}\n"
		sudo xbps-install xorg-minimal budgie-desktop gnome-control-center gnome-system-monitor gnome-terminal nautilus \
			nautilus-sendto gnome-keyring evince gedit gedit-plugins eog eog-plugins gnome-screenshot \
			gnome-disk-utility gvfs gvfs-mtp gvfs-gphoto2 file-roller
		echo -e "\n${c2}Done${n}\n"
		;;

		6 )
# Cinnamon
		echo -e "\n${c4}Install Cinnamon...${n}\n"
		sudo xbps-install xorg-minimal cinnamon gnome-system-monitor gnome-terminal gnome-screenshot gnome-disk-utility \
			gnome-keyring gedit gedit-plugins evince gvfs gvfs-mtp gvfs-gphoto2 eog eog-plugins file-roller
		echo -e "\n${c2}Done${n}\n"
		;;

		7 )
# Lxqt
		echo -e "\n${c4}Install LXQt...${n}\n"
		sudo xbps-install xorg-minimal lxqt-about lxqt-admin lxqt-archiver lxqt-build-tools lxqt-config lxqt-globalkeys \
			lxqt-openssh-askpass lxqt-panel lxqt-policykit lxqt-powermanagement lxqt-qtplugin lxqt-runner lxqt-session \
			lxqt-sudo lxqt-themes obconf-qt openbox pcmanfm-qt lximage-qt FeatherPad qlipper qterminal lxqt-notificationd
		echo -e "\n${c2}Done${n}\n"
		;;

		8 )
# Enlightenment
		echo -e "\n${c4}Install Enlightenment...${n}\n"
		sudo xbps-install enlightenment terminology mousepad gvfs gvfs-mtp gvfs-gphoto2 zathura zathura-pdf-poppler \
			Thunar thunar-volman thunar-archive-plugin thunar-media-tags-plugin xarchiver
		echo -e "\n${c2}Done${n}\n"
		;;

		9 )
# Lxde
		echo -e "\n${c4}Install LXDE...${n}\n"
		sudo xbps-install xorg-minimal lxde-common lxde-icon-theme lxappearance lxinput lxpanel lxrandr lxsession lxtask \
			lxterminal pcmanfm gvfs gvfs-mtp gvfs-gphoto2 viewnior mousepad zathura zathura-pdf-poppler \
			openbox obconf xarchiver
		echo -e "\n${c2}Done${n}\n"
		;;

		10 )
# Lumina
		echo -e "\n${c4}Install Lumina...${n}\n"
		sudo xbps-install xorg-minimal lumina lumina-pdf lumina-calculator gvfs gvfs-mtp gvfs-gphoto2 mousepad viewnior
		echo -e "\n${c2}Done${n}\n"
		;;

	esac

	;;

# Window manager

	2 )
	echo -e "\n${c4}Install a Window Manager...${n}\n"
	printf "Possible (type in number): \n
	--- Xorg ---
	- 1 i3-gaps
	- 2 Openbox
	- 3 Fluxbox
	- 4 bspwm
	- 5 herbstluftwm
	- 6 IceWM
	- 7 awesome
	- 8 jwm
	- 9 dwm
	- 10 FVWM3
	- 11 Qtile
	--- Wayland ---
	- 12 Sway
	- 13 Wayfire\n"
	read -p "Which Window manager do you want? " windowmanager
	case $windowmanager in
		1 )
# i3-gaps
		echo -e "\n${c4}Install i3-gaps...${n}\n"
		sudo xbps-install xorg-minimal xorg-video-drivers xorg-input-drivers i3-gaps i3lock i3status i3blocks dunst dmenu feh Thunar \
			thunar-volman thunar-archive-plugin thunar-media-tags-plugin xarchiver \
			lm_sensors acpi playerctl scrot htop arandr gvfs gvfs-mtp \
			gvfs-gphoto2 xfce4-taskmanager viewnior
		echo -e "\n${c2}Done${n}\n"
		;;

		2 )
# Openbox
		echo -e "\n${c4}Install Openbox...${n}\n"
		sudo xbps-install xorg-minimal xorg-video-drivers xorg-input-drivers openbox obconf lxappearance jgmenu dunst \
			feh lxrandr lxinput pcmanfm gvfs gvfs-mtp gvfs-gphoto2 \
			lxtask scrot htop xarchiver viewnior tint2conf obmenu-generator
		echo -e "\n${c2}Done${n}\n"
		;;

		3 )
# Fluxbox
		echo -e "\n${c4}Install Fluxbox...${n}\n"
		sudo xbps-install xorg-minimal xorg-video-drivers xorg-input-drivers fluxbox dunst feh arandr Thunar thunar-volman \
		 thunar-archive-plugin thunar-media-tags-plugin gvfs gvfs-mtp gvfs-gphoto2 scrot htop xarchiver viewnior
		echo -e "\n${c2}Done${n}\n"
		;;

		4 )
# Bspwm
		echo -e "\n${c4}Install Bspwm...${n}\n"
		sudo xbps-install xorg-minimal xorg-video-drivers xorg-input-drivers bspwm sxhkd dunst feh dmenu arandr Thunar thunar-volman \
			thunar-archive-plugin thunar-media-tags-plugin gvfs gvfs-mtp gvfs-gphoto2 scrot htop xarchiver viewnior
		echo -e "\n${c2}Done${n}\n"
		;;

		5 )
# Herbsluftwm
		echo -e "\n${c4}Install herbstluftwm...${n}\n"
		sudo xbps-install xorg-minimal xorg-video-drivers xorg-input-drivers herbstluftwm dunst feh dmenu arandr Thunar thunar-volman \
			thunar-archive-plugin thunar-media-tags-plugin gvfs gvfs-mtp gvfs-gphoto2 scrot htop xarchiver viewnior
		echo -e "\n${c2}Done${n}\n"
		;;

		6 )
# Icewm
		echo -e "\n${c4}Install IceWM...${n}\n"
		sudo xbps-install xorg-minimal xorg-video-drivers xorg-input-drivers icewm dunst feh dmenu arandr Thunar thunar-volman \
			thunar-archive-plugin thunar-media-tags-plugin gvfs gvfs-mtp gvfs-gphoto2 scrot htop xarchiver viewnior
		echo -e "\n${c2}Done${n}\n"
		;;

		7 )
# Awesome
		echo -e "\n${c4}Install Awesome...${n}\n"
		sudo xbps-install xorg-minimal xorg-video-drivers xorg-input-drivers awesome vicious dunst feh arandr Thunar thunar-volman \
			thunar-archive-plugin thunar-media-tags-plugin gvfs gvfs-mtp gvfs-gphoto2 scrot \
			htop xarchiver viewnior
		echo -e "\n${c2}Done${n}\n"
		;;

		8 )
# Jwm
		echo -e "\n${c4}Install jwm...${n}\n"
		sudo xbps-install xorg-minimal xorg-video-drivers xorg-input-drivers jwm dunst feh dmenu arandr Thunar thunar-volman \
			thunar-archive-plugin thunar-media-tags-plugin gvfs gvfs-mtp gvfs-gphoto2 scrot htop xarchiver viewnior
		echo -e "\n${c2}Done${n}\n"
		;;

		9 )
# Dwm
		echo -e "\n${c4}Install dwm...${n}\n"
		sudo xbps-install xorg-minimal xorg-video-drivers xorg-input-drivers dwm dunst feh dmenu arandr Thunar thunar-volman \
			thunar-archive-plugin thunar-media-tags-plugin gvfs gvfs-mtp gvfs-gphoto2 scrot htop xarchiver viewnior
		echo -e "\n${c2}Done${n}\n"
		;;

		10 )
# FVWM3
		echo -e "\n${c4}Install FVWM3...${n}\n"
		sudo xbps-install xorg-minimal xorg-video-drivers xorg-input-drivers fvwm3 feh arandr Thunar thunar-volman \
			thunar-archive-plugin thunar-media-tags-plugin gvfs gvfs-mtp gvfs-gphoto2 scrot htop xarchiver viewnior
		echo -e "\n${c2}Done${n}\n"
		;;

		11 )
# Qtile
		echo -e "\n${c4}Install Qtile...${n}\n"
		sudo xbps-install python3 python3-pip python3-setuptools python3-wheel python3-virtualenv-clone python3-dbus \
			python3-gobject pango pango-devel libffi-devel xcb-util-cursor gdk-pixbuf
#		sudo xbps-install xorg-minimal xorg-video-drivers xorg-input-drivers feh arandr Thunar thunar-volman \
			thunar-archive-plugin thunar-media-tags-plugin gvfs gvfs-mtp gvfs-gphoto2 scrot htop xarchiver viewnior
		pip install qtile
		echo -e "\n${c2}Done${n}\n"
		;;

		12 )
# Sway
		echo -e "\n${c4}Install Sway...${n}\n"
		sudo xbps-install wayland elogind dbus-elogind seatd sway swaybg swayidle swaylock azote grimshot Waybar \
		gvfs gvfs-mtp gvfs-gphoto2 htop wofi xf86-video-qxl mesa-dri
		echo -e "\n${c2}Done${n}\n"
		;;

		13 )
# Wayfire
		echo -e "\n${c4}Install Wayfire...${n}\n"
		sudo xbps-install wayland wayfire grim gvfs gvfs-mtp gvfs-gphoto2 htop wofi mesa-dri
		echo -e "\n${c2}Done${n}\n"
		;;

	esac

	;;

	3 )
# Xorg
	echo -e "\n${c4}Install the X Window System only...${n}\n"
	sudo xbps-install xorg-server xorg-server-xwayland xorg-video-drivers xorg-input-drivers \
		xinit xauth xrandr xrdb xwininfo xdpyinfo xsetroot neofetch
	echo -e "\n${c4}Copy configurations...${n}\n"
	if [ ! -d /etc/X11/xorg.conf.d ]; then
		sudo mkdir -p /etc/X11/xorg.conf.d
	fi
	echo -e "\n${c2}Done${n}\n"
	;;

	4 )
# Wayland
	sudo xbps-install wayland
	;;

esac

# Install display manager

case $gui in
	2 )
	echo -e "\n${c4}Install a Display manageer...${n}\n"
	printf "Possible (type in number): \n
	- 1 Lightdm
	- 2 Emptty
	- 3 Slim\n"
	read -p "Which display manager do you want? " displaymanager
	case $displaymanager in
		1 )
		echo -e "\n${c4}Install Lightdm...${n}\n"
		sudo xbps-install lightdm lightdm-gtk3-greeter lightdm-gtk-greeter-settings
		echo -e "\n${c2}Done${n}\n"
		;;

		2 )
		echo -e "\n${c4}Install Emptty...${n}\n"
		sudo xbps-install emptty
		echo -e "\n${c2}Done${n}\n"
		;;

		3 )
		echo -e "\n${c4}Install Slim...${n}\n"
		sudo xbps-install slim slim-void-theme
		sudo cp conf.slim /etc/conf.slim
		echo -e "\n${c2}Done${n}\n"
		;;

	esac

# Install a Terminal emulator

	echo -e "\n${c4}Install a Terminal emulator...${n}\n"
	printf "Possible (type in number): \n
	- 1 Alacritty
	- 2 xterm
	- 3 LXTerminal
	- 4 Yakuake
	- 5 Sakura
	- 6 Kitty\n"
	read -p "What terminal emulator do you want? " terminal
	case $terminal in
		1 )
		echo -e "\n${c4}Install Alacritty...${n}\n"
		sudo xbps-install alacritty alacritty-terminfo
		export TERMINAL="alacritty"
		printf TERM="alacritty" > ~/.bashrc
		term=alacritty
		echo -e "\n${c2}Done${n}\n"
		;;

		2 )
		echo -e "\n${c4}Install xterm...${n}\n"
		sudo xbps-install xterm
		export TERMINAL="xterm"
		printf TERM="xterm" > ~/.bashrc
		term=xterm
		echo -e "\n${c2}Done${n}\n"
		;;

		3 )
		echo -e "\n${c4}Install LXTerminal...${n}\n"
		sudo xbps-install lxterminal
		export TERMINAL="lxterminal"
		printf TERM="lxterminal" > ~/.bashrc
		term=lxterminal
		echo -e "\n${c2}Done${n}\n"
		;;

		4 )
		echo -e "\n${c4}Install Yakuake...${n}\n"
		sudo xbps-install yakuake
		export TERMINAL="yakuake"
		printf TERM="yakuake" > ~/.bashrc
		term=yakuake
		echo -e "\n${c2}Done${n}\n"
		;;

		5 )
		echo -e "\n${c4}Install Sakura...${n}\n"
		sudo xbps-install sakura
		export TERMINAL="sakura"
		printf TERM="sakura" > ~/.bashrc
		term=sakura
		echo -e "\n${c2}Done${n}\n"
		;;

		6 )
		echo -e "\n${c4}Install Kitty...${n}\n"
		sudo xbps-install kitty kitty-terminfo
		export TERMINAL="kitty"
		printf TERM="kitty" > ~/.bashrc
		term=kitty
		echo -e "\n${c2}Done${n}\n"
		;;

	esac

# Install terminal text editor

	echo -e "\n${c4}Choose terminal text editor...${n}\n"
	printf "Possible (type in number): \n
	- 1 Emacs
	- 2 Micro
	- 3 Nano
	- 4 Vim\n"
	read -p "Which terminal text editor do you want?" editor
	case $editor in
		1 )
		echo -e "\n${c4}Install Emacs...${n}\n"
		sudo xbps-install emacs
		;;

		2 )
		echo -e "\n${c4}Install Micro...${n}\n"
		sudo xbps-install micro
		;;

		3 )
		echo -e "\n${c4}Install Nano...${n}\n"
		sudo xbps-install nano
		;;

		4 )
		echo -e "\n${c4}Install Vim...${n}\n"
		sudo xbps-install vim vim-colorschemes
		;;

	esac

# Install GUI Text editor

	echo -e "\n${c4}Choose GUI text editor...${n}\n"
	printf "Possible (type in number): \n
	- 1 Geany
	- 2 Gedit
	- 3 Kate
	- 4 Sublime
	- 5 LeafPad
	- 6 Mousepad
	- 7 Atom
	- 8 Code-OSS
	- 9 Notepadqq
	- 10 Bluefish
	- 11 Emacs gtk2
	- 12 Emacs gtk3
	- 13 Emacs x11
	- 14 Qemacs
	- 15 Vile
	- 16 Zile
	- 17 Gvim
	- 18 Neovim
	- 19 Kakoune\n"
	read -p "Which GUI Text editor do you want?" geditor
	case $geditor in
		1 )
		echo -e "\n${c4}Install Geany...${n}\n"
		sudo xbps-install geany geany-plugins geany-plugins-extra
		;;

		2 )
		echo -e "\n${c4}Install Gedit...${n}\n"
		sudo xbps-install gedit gedit-plugins
		;;

		3 )
		echo -e "\n${c4}Install Kate...${n}\n"
		sudo xbps-install kate5
		;;

		4 )
		echo -e "\n${c4}Install Sublime...${n}\n"
		sudo xbps-install sublime-text3
		;;

		5 )
		echo -e "\n${c4}Install LeafPad...${n}\n"
		sudo xbps-install leafpad
		;;

		6 )
		echo -e "\n${c4}Install Mousepad...${n}\n"
		sudo xbps-install mousepad
		;;

		7 )
		echo -e "\n${c4}Install Atom...${n}\n"
		sudo xbps-install atom
		;;

		8 )
		echo -e "\n${c4}Install Code-OSS...${n}\n"
		sudo xbps-install vscode
		;;

		9 )
		echo -e "\n${c4}Install Notepadqq...${n}\n"
		sudo xbps-install notepadqq
		;;

		10 )
		echo -e "\n${c4}Install Bluefish...${n}\n"
		sudo xbps-install bluefish
		;;

		11 )
		echo -e "\n${c4}Install Emacs gtk2...${n}\n"
		sudo xbps-install emacs-gtk2
		;;

		12 )
		echo -e "\n${c4}Install Emacs gtk3...${n}\n"
		sudo xbps-install emacs-gtk3
		;;

		13 )
		echo -e "\n${c4}Install Emacs x11...${n}\n"
		sudo xbps-install emacs-x11
		;;

		14 )
		echo -e "\n${c4}Install Qemacs...${n}\n"
		sudo xbps-install qemacs
		;;

		15 )
		echo -e "\n${c4}Install Vile...${n}\n"
		sudo xbps-install vile
		;;

		16 )
		echo -e "\n${c4}Install Zile...${n}\n"
		sudo xbps-install zile
		;;

		17 )
		echo -e "\n${c4}Install Gvim...${n}\n"
		sudo xbps-install gvim
		;;

		18 )
		echo -e "\n${c4}Install Neovim...${n}\n"
		sudo xbps-install neovim
		;;

		19 )
		echo -e "\n${c4}Install Kakoune...${n}\n"
		sudo xbps-install kakoune
		;;

	esac

# Install an Internet browser

	echo -e "\n${c4}Install an Internet browser...${n}\n"
	printf "Possible (type in number): \n
	- 1 Firefox
	- 2 Firefox-ESR
	- 3 Chromium
	- 4 qutebrowser
	- 5 Falkon
	- 6 Badwolf\n"
	read -p "Which browser do you want? " browser
	case $browser in
		1 )
		echo -e "\n${c4}Install Firefox...${n}\n"
		sudo xbps-install firefox firefox-i18n-en-US firefox-i18n-de
		echo -e "\n${c2}Done${n}\n"
		;;

		2 )
		echo -e "\n${c4}Install Firefox Extended Support Release...${n}\n"
		sudo xbps-install firefox-esr firefox-esr-i18n-en-US firefox-esr-i18n-de
		echo -e "\n${c2}Done${n}\n"
		;;

		3 )
		echo -e "\n${c4}Install Chromium...${n}\n"
		sudo xbps-install chromium
		echo -e "\n${c2}Done${n}\n"
		;;

		4 )
		echo -e "\n${c4}Install qutebrowser...${n}\n"
		sudo xbps-install qutebrowser
		echo -e "\n${c2}Done${n}\n"
		;;

		5 )
		echo -e "\n${c4}Install Falkon...${n}\n"
		sudo xbps-install falkon
		echo -e "\n${c2}Done${n}\n"
		;;

		6 )
		echo -e "\n${c4}Install Badwolf...${n}\n"
		sudo xbps-install badwolf
		echo -e "\n${c2}Done${n}\n"
		;;

	esac

# Install a media player

	echo -e "\n${c4}Install a Video player...${n}\n"
	printf "Possible (type in number): \n
	- 1 mpv
	- 2 VLC Media Player
	- 3 Parole (part of Xfce)
	- 4 Totem (part of GNOME)\n"
	read -p "Which Video player do you want? " mediaplayer
	case $mediaplayer in
		1 )
		echo -e "\n${c4}Install mpv...${n}\n"
		sudo xbps-install mpv
		echo -e "\n${c2}Done${n}\n"
		;;

		2 )
		echo -e "\n${c4}Install VLC Media Player...${n}\n"
		sudo xbps-install vlc
		echo -e "\n${c2}Done${n}\n"
		;;

		3 )
		echo -e "\n${c4}Install Parole...${n}\n"
		sudo xbps-install parole
		echo -e "\n${c2}Done${n}\n"
		;;

		4 )
		echo -e "\n${c4}Install Totem...${n}\n"
		sudo xbps-install totem
		echo -e "\n${c2}Done${n}\n"
		;;

	esac
	;;

esac

# Install fonts

	read -p "Do you want to install some fonts? (y) " fonts
	case $fonts in
		y )
		echo -e "\n${c4}Install fonts...${n}\n"
		sudo xbps-install liberation-fonts-ttf dejavu-fonts-ttf \
			ttf-ubuntu-font-family fonts-roboto-ttf
		echo -e "\n${c2}Done${n}\n"
		;;

	esac


# Install LibreOffice

read -p "Do you want to install LibreOffice? (y) " libreoffice
case $libreoffice in
	y )
	echo -e "\n${c4}Install LibreOffice...${n}\n"
	sudo xbps-install libreoffice-writer libreoffice-calc libreoffice-impress \
		libreoffice-draw libreoffice-math libreoffice-base libreoffice-gnome \
		libreoffice-i18n-en-US libreoffice-i18n-de
	echo -e "\n${c2}Done${n}\n"
	;;

esac

# Install GIMP + Inkscape

read -p "Do you want to install GIMP and Inkscape? (y) " gimpinkscape
case $gimpinkscape in
	y )
	echo -e "\n${c4}Install GIMP and Inkscape...${n}\n"
	sudo xbps-install inkscape gimp
	echo -e "\n${c2}Done${n}\n"
	;;

esac

# Install QEMU + Virt Manager

read -p "Do you want to install QEMU and Virt Manager? (y) " qemuvirt
case $qemuvirt in
	y )
	echo -e "\n${c4}Install QEMU and Virt Manager...${n}\n"
	sudo xbps-install qemu virt-manager libvirt
	echo -e "\n${c4}Enable libvirtd service...${n}\n"
	if [ -L /var/service/libvirtd ]; then
		echo -e "\n${c2}Service libvirtd already exist. Continue.${n}\n"
	else
		sudo ln -sv /etc/sv/libvirtd /var/service
		echo -e "\n${c2}Done${n}\n"
	fi
	;;

esac

# Install a Backup program
echo -e "\n${c4}Install a Backup program...${n}\n"
printf "Possible (type in number): \n
- 1 Borg
- 2 Timeshift
- 3 Deja-dup\n"
read -p "Which Backup program do you want? " backup
case $backup in
	1 )
	echo -e "\n${c4}Install Borg...${n}\n"
	sudo xbps-install borg
	echo -e "\n${c2}Done${n}\n"
	;;

	2 )
	echo -e "\n${c4}Install Timeshift...${n}\n"
	sudo xbps-install timeshift
	echo -e "\n${c2}Done${n}\n"
	;;

	3 )
	echo -e "\n${c4}Install Deja-dup...${n}\n"
	sudo xbps-install deja-dup
	echo -e "\n${c2}Done${n}\n"
	;;

	esac

# Choose X keyboard language

	echo -e "\n${c4}Change X keyboard language...${n}\n"
	printf "Possible (type in number)or Enter for English: \n
	- 1 Czech
	- 2 Deutsch
	- 3 French
	- 4 Russian
	- 5 Spanish\n"
	echo -n "$b"
	read -p "Which keyboard layout do you want? " xlanguage
	echo -n "$none"
	case $xlanguage in
		1 )
			if [ ! -d /etc/X11/xorg.conf.d ]; then
				sudo mkdir -p /etc/X11/xorg.conf.d
			fi
			sed "s/"cs"/"cz"/" conf.keyboard > 00-keyboard.conf
			sudo cp -r 00-keyboard.conf /etc/X11/xorg.conf.d/00-keyboard.conf
			sudo cp -r conf.touchpad /etc/X11/xorg.conf.d/20-touchpad.conf
			echo -e "\n${c2}Done${n}\n"
			;;

		2 )
			if [ ! -d /etc/X11/xorg.conf.d ]; then
				sudo mkdir -p /etc/X11/xorg.conf.d
			fi
			sed "s/"cs"/"de"/" conf.keyboard > 00-keyboard.conf
			sudo cp -r 00-keyboard.conf /etc/X11/xorg.conf.d/00-keyboard.conf
			sudo cp -r conf.touchpad /etc/X11/xorg.conf.d/20-touchpad.conf
			echo -e "\n${c2}Done${n}\n"
			;;

		3 )
			if [ ! -d /etc/X11/xorg.conf.d ]; then
				sudo mkdir -p /etc/X11/xorg.conf.d
			fi
			sed "s/"cs"/"fr"/" conf.keyboard > 00-keyboard.conf
			sudo cp -r 00-keyboard.conf /etc/X11/xorg.conf.d/00-keyboard.conf
			sudo cp -r conf.touchpad /etc/X11/xorg.conf.d/20-touchpad.conf
			echo -e "\n${c2}Done${n}\n"
			;;

		4 )
			if [ ! -d /etc/X11/xorg.conf.d ]; then
				sudo mkdir -p /etc/X11/xorg.conf.d
			fi
			sed "s/"cs"/"ru"/" conf.keyboard > 00-keyboard.conf
			sudo cp -r 00-keyboard.conf /etc/X11/xorg.conf.d/00-keyboard.conf
			sudo cp -r conf.touchpad /etc/X11/xorg.conf.d/20-touchpad.conf
			echo -e "\n${c2}Done${n}\n"
			;;


		5 )
			if [ ! -d /etc/X11/xorg.conf.d ]; then
				sudo mkdir -p /etc/X11/xorg.conf.d
			fi
			sed "s/"cs"/"es"/" conf.keyboard > 00-keyboard.conf
			sudo cp -r 00-keyboard.conf /etc/X11/xorg.conf.d/00-keyboard.conf
			sudo cp -r conf.touchpad /etc/X11/xorg.conf.d/20-touchpad.conf
			echo -e "\n${c2}Done${n}\n"
			;;


		* )
			if [ ! -d /etc/X11/xorg.conf.d ]; then
				sudo mkdir -p /etc/X11/xorg.conf.d
			fi
			sed "s/"cs"/"us"/" conf.keyboard > 00-keyboard.conf
			sudo cp -r 00-keyboard.conf /etc/X11/xorg.conf.d/00-keyboard.conf
			sudo cp -r conf.touchpad /etc/X11/xorg.conf.d/20-touchpad.conf
			echo -e "\n${c2}Done${n}\n"
			;;
		esac

# Enable required services

echo -e "\n${c4}Enable required services..."
echo "Enable D-Bus...${n}\n"
sudo xbps-install -y dbus
if [ -L /var/service/dbus ]; then
	echo -e "\n${c2}Service dbus already exist. Continue.${n}\n"
else
	sudo ln -s /etc/sv/dbus /var/service
	echo -e "\n${c2}Done${n}\n"
fi
	echo -e "\n${c4}Enable elogind...${n}\n"
	sudo xbps-install -y elogind
if [ -L /var/service/elogind ]; then
	echo -e "\n${c2}Service elogind already exist. Continue.${n}\n"
else
	sudo ln -s /etc/sv/elogind /var/service
	echo -e "\n${c2}Done${n}\n"
fi
	echo -e "\n${c4}Enable Polkit...${n}\n"
if [ -L /var/service/polkitd ]; then
	echo -e "\n${c2}Service polkitd already exist. Continue.${n}\n"
else
	sudo ln -s /etc/sv/polkitd /var/service
	echo -e "\n${c2}Done${n}\n"
fi

# Configure Cron

echo -e "\n${c4}Install cronie...${n}\n"
sudo xbps-install -y cronie
echo -e "\n${c4}Enable cronie service...${n}\n"
if [ -L /var/service/cronie ]; then
	echo -e "\n${c2}Service cronie already exist. Continue.${n}\n"
else
	sudo ln -sv /etc/sv/cronie /var/service
	echo -e "\n${c2}Done${n}\n"
fi

# Configure Audio

read -p "Do you want to install PulseAudio for audio? (y) " pulseaudio
case $pulseaudio in
	y )
	echo -e "\n${c4}Install PulseAudio...${n}\n"
	sudo xbps-install pulseaudio pulseaudio-utils pulsemixer alsa-plugins-pulseaudio \
		pavucontrol
	echo -e "\n${c2}Done${n}\n"
	;;

esac

# Configure Network Management

echo -e "\n${c4}Configure Network Management...${n}\n"
printf "\nPossible (type in number): \n
- 1 Network Manager
- 2 Connman\n"
read -p "Which Network Management tool do you want to use? " netmngt
case $netmngt in
	1 )
	echo -e "\n${c4}Install Network Manager...${n}\n"
	sudo xbps-install NetworkManager NetworkManager-openvpn NetworkManager-openconnect \
		NetworkManager-vpnc NetworkManager-l2tp
	read -p "Do you want to integrate Network Manager into a graphical environment? (y) " nmapplet
	case $nmapplet in
		y )
		echo -e "\n${c4}Service Install Network Manager applet...${n}\n"
		sudo xbps-install network-manager-applet
		;;

	esac

	echo -e "\n${c4}Enable Network Manager service...${n}\n"
	if [ -L /var/service/NetworkManager ]; then
		echo -e "\n${c2}Service NetworkManager already exist. Continue.${n}\n"
	else
		sudo ln -sv /etc/sv/NetworkManager /var/service
		echo -e "\n${c2}Done${n}\n"
	fi
	;;

	2 )
	echo -e "\n${c4}Install Connman...${n}\n"
	sudo xbps-install connman connman-ncurses
	read -p "Do you want to integrate Connman into a graphical environment? (y) " connapplet
	case $connapplet in
		y )
		echo -e "\n${c4}Service Install Connman GTK...${n}\n"
		sudo xbps-install connman-ui
		;;

	esac

	echo -e "\n${c4}Enable Connman service...${n}\n"
	if [ -L /var/service/connmand ]; then
		echo -e "\n${c2}Service connmand already exist. Continue.${n}\n"
	else
		sudo ln -sv /etc/sv/connmand /var/service
		echo -e "\n${c2}Done${n}\n"
	fi
	;;

esac

# Configure Bluetooth

read -p "Do you want to use Bluetooth? (y) " bluetooth
case $bluetooth in
	y )
	echo -e "\n${c4}Install BlueZ...${n}\n"
	sudo xbps-install bluez
	printf "\n"
	read -p "Do you want to integrate Bluetooth into a graphical environment? (y) " blueman
	case $blueman in
		y )
		echo -e "\n${c4}Install Blueman...${n}\n"
		sudo xbps-install blueman
		;;

	esac

	echo -e "\n${c4}Enable Bluetooth service...${n}\n"
	if [ -L /var/service/bluetoothd ]; then
		echo -e "\n${c2}Service bluetoothd already exist. Continue.${n}\n"
	else
		sudo ln -sv /etc/sv/bluetoothd /var/service
		echo -e "\n${c2}Done${n}\n"
	fi
	;;

esac

# Configure Printing support

read -p "Do you want to use printers? (y) " printer
case $printer in
	y )
	echo -e "\n${c4}Install CUPS and Tools...${n}\n"
	sudo xbps-install cups cups-pk-helper cups-filters foomatic-db foomatic-db-engine
	read -p "Do you want to install a graphical printer program? (y) " printergui
	case $printergui in
		y )
		echo -e "\n${c4}Install system-config-printer...${n}\n"
		sudo xbps-install system-config-printer
		;;

	esac

	echo -e "\n${c4}Enable CUPS service...${n}\n"
	if [ -L /var/service/cupsd ]; then
		echo -e "\n${c2}Service cupsd already exist. Continue.${n}\n"
	else
		sudo ln -sv /etc/sv/cupsd /var/service
		echo -e "\n${c2}Done${n}\n"
	fi
	;;

esac

# Configure Notebook Power Saving

read -p "Do you want to install TLP for power saving (Notebooks only)? (y) " nb_power
case $nb_power in
	y )
	echo -e "\n${c4}Install TLP and PowerTop...${n}\n"
	sudo xbps-install tlp tlp-rdw powertop
	echo -e "\n${c4}Enable TLP service...${n}\n"
	if [ -L /var/service/tlp ]; then
		echo -e "\n${c2}Service tlp already exist. Continue.${n}\n"
	else
		sudo ln -sv /etc/sv/tlp /var/service
		echo -e "\n${c2}Done${n}\n"
	fi
	;;

esac

# Configure Filesharing

read -p "Do you want to install NFS for file sharing? (y) " pulseaudio
case $pulseaudio in
	y )
	echo -e "\n${c4}Install NFS...${n}\n"
	sudo xbps-install nfs-utils sv-netmount
	sudo ln -s /etc/sv/statd /var/service/
	sudo ln -s /etc/sv/rcpbind /var/service/
	sudo ln -s /etc/sv/netmount /var/service/
	echo -e "\n${c2}Done${n}\n"
	;;

esac


# Configure the window manager

read -p "Do you want to configure your Window Manager? (y) " confwm
case $confwm in
	y )
	echo -e "\n${c4}Will be configured just for your user...${n}\n"
	 case $windowmanager in
		1 )
# i3-gaps
		echo -e "\n${c4}Configuring i3-gaps...${n}\n"
		echo 'exec i3-gaps-session' > .xinitrc
		echo -e "\n${c2}Done${n}\n"
		;;

		2 )
# Openbox
		echo -e "\n${c4}Configuring Openbox...${n}\n"
		obmenu-generator -p -i -u -d -c
		echo 'exec openbox-session' > .xinitrc
		echo -e "\n${c2}Done${n}\n"
		;;

		3 )
# Fluxbox
		echo -e "\n${c4}Configuring Fluxbox...${n}\n"
		echo 'exec fluxbox-session' > .xinitrc
		echo -e "\n${c2}Done${n}\n"
		;;

		4 )
# Bspwm
		echo -e "\n${c4}Configuring Bspwm...${n}\n"
		echo 'exec bspwm-session' > .xinitrc
		echo -e "\n${c2}Done${n}\n"
		;;

		5 )
# Herbsluftwm
		echo -e "\n${c4}Configuring herbstluftwm...${n}\n"
		echo 'exec herbsluftwm-session' > .xinitrc
		echo -e "\n${c2}Done${n}\n"
		;;

		6 )
# Icewm
		echo -e "\n${c4}Configuring IceWM...${n}\n"
		echo 'exec icewm-session' > .xinitrc
		echo -e "\n${c2}Done${n}\n"
		;;

		7 )
# Awesome
		echo -e "\n${c4}Configuring Awesome...${n}\n"
		mkdir ~/.config/awesome
		sed "s/"xterm"/"$term"/" /etc/xdg/awesome/rc.lua > ~/.config/awesome/rc.lua
		echo 'exec awesome-session' > .xinitrc
		echo -e "\n${c2}Done${n}\n"
		;;

		8 )
# Jwm
		echo -e "\n${c4}Configuring jwm...${n}\n"
		echo 'exec jwm-session' > .xinitrc
		echo -e "\n${c2}Done${n}\n"
		;;

		9 )
# Dwm
		echo -e "\n${c4}Configuring dwm...${n}\n"
		echo 'exec dwm-session' > .xinitrc
		echo -e "\n${c2}Done${n}\n"
		;;

		10 )
# FVWM3
		echo -e "\n${c4}Configuring FVWM3...${n}\n"
		echo 'exec fvwm3-session' > .xinitrc
		echo -e "\n${c2}Done${n}\n"
		;;

		11 )
# Qtile
		echo -e "\n${c4}Configuring Qtile...${n}\n"
		echo 'exec qtile-session' > .xinitrc
		echo -e "\n${c2}Done${n}\n"
		;;

		12 )
# Sway
		echo -e "\n${c4}Configuring Sway...${n}\n"
		sudo ln -s /etc/sv/seatd /var/service
		usermod -aG _seatd "$USER"
		echo 'exec sway-session' > .xinitrc
		echo -e "\n${c2}Done${n}\n"
		;;

		13 )
# Wayfire
		echo -e "\n${c4}Configuring Wayfire...${n}\n"
		echo 'exec wayfire-session' > .xinitrc
		echo -e "\n${c2}Done${n}\n"
		;;

	esac

	;;

esac

# Configure the Display manager

read -p "Check if a display manager exist. If yes, enable it? (y) " dmenable
case $dmenable in
	y )
	if [ -f /usr/bin/lightdm ]; then
		sudo ln -sv /etc/sv/lightdm /var/service
	elif [ -f /usr/bin/sddm ]; then
		sudo ln -sv /etc/sv/sddm /var/service
	elif [ -f /usr/bin/gdm ]; then
		sudo ln -sv /etc/sv/gdm /var/service
	elif [ -f /usr/bin/slim ]; then
		sudo ln -sv /etc/sv/slim /var/service
	elif [ -f /usr/bin/emptty ]; then
		sudo ln -sv /etc/sv/emptty /var/service
	fi
	;;

esac

echo -e "\n${c1}Installation finished. Enjoy Void linux...${n}\n"
