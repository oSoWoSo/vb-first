#!/usr/bin/bash

#
# Get latest OVMF script for vb aka VoidGNU/Linux
#
# Copyright (c) 2022 zenobit from oSoWoSo
# mail: <zen@osowoso.xyz> web: https://osowoso.xyz
# licensed under EUPL 1.2
# source:
# 

echo -e "\e[32m Getting latest ovmf from kraxel.org\e[0m"
wget -m -np -nd -A "edk2.git-ovmf-x64*.noarch.rpm" https://www.kraxel.org/repos/jenkins/edk2/
mv *.noarch.rpm edk2.git-ovmf-x64.noarch.rpm

if [ -e /bin/rpmextract ]
then
	rpmextract edk2.git-ovmf-x64.noarch.rpm
else
	xbps-install -y rpmextract
    rpmextract edk2.git-ovmf-x64.noarch.rpm
    xbps-remove -y rpmextract
fi

cp -rv usr/share /usr/

echo -e "\e[32m Script finished\e[0m"
