#!/usr/bin/bash

#
#  script for vb aka VoidGNU/Linux
#
# Copyright (c) 2022 zenobit from oSoWoSo
# mail: <zen@osowoso.xyz> web: https://osowoso.xyz
# licensed under EUPL 1.2
# source:
#

# VBM color definitions
numc0=0
numc1=1
numc2=2
numc3=3
numc4=4
numc5=5
numc6=6
numc7=7
numc8=8
# second definition
c0=${numc0}
c1=${numc1}
c2=${numc2}
c3=${numc3}
c4=${numc4}
c5=${numc5}
c6=${numc6}
c7=${numc7}
c8=${numc8}
## COLORS
c0=$(tput setaf 0) # black
c1=$(tput setaf 1) # red
c2=$(tput setaf 2) # green
c3=$(tput setaf 3) # yellow
c4=$(tput setaf 4) # blue
c5=$(tput setaf 5) # magenta
c6=$(tput setaf 6) # light blue
c7=$(tput setaf 7) # white
c8=$(tput setaf 8) # grey
c=$(tput sgr0) # reset
# colortest
echo "${c0}" testblack
echo "${c1}" testred
echo "${c2}" testgreen
echo "${c3}" testyellow
echo "${c4}" testblue
echo "${c5}" testmagenta
echo "${c6}" testlightblue
echo "${c7}" testwhite
echo "${c8}" testgrey
echo "${c}" testclear
# underline
u=$"{[0;4m}"
b=$"{[0;1m}"
bu=$"{[1;4m}"

# program name
progname=${0##*/}

## COLORS
c0=$(tput setaf 0) # black
c1=$(tput setaf 1) # red
c2=$(tput setaf 2) # green
c3=$(tput setaf 3) # yellow
c4=$(tput setaf 4) # blue
c5=$(tput setaf 5) # 
c6=$(tput setaf 6) # light blue
c7=$(tput setaf 7) # white
c8=$(tput setaf 8) # grey
c9=$(tput setaf 9) # 
n=$(tput sgr0)
# colortest
echo "${c0}" test
echo "${c1}" test
echo "${c2}" test
echo "${c3}" test
echo "${c4}" test
echo "${c5}" test
echo "${c6}" test
echo "${c7}" test
echo "${c8}" test
echo "${c8}" test
echo "${n}" test
# background
b0=$(tput setab 0)
b1=$(tput setab 1)
b2=$(tput setab 2)
b3=$(tput setab 3)
b4=$(tput setab 4)
b5=$(tput setab 5)
b6=$(tput setab 6)
b7=$(tput setab 7)
b8=$(tput setab 8)
b9=$(tput setab 9)

## check
if [ -f /bin/***changme*** ]; then
	echo -e "${c2}***changme***${n}"
else
	echo -e "${c2}***changme***${n}"
	sudo xbps-install ***changme***
fi

## rootcheck
check_root () {
	if [[ $EUID -gt 0 ]]; then
		echo -e "\n${c1}This operation needs super-user privileges."
		echo -e "${c2}exiting...${n}"
		exit 255
	fi
}
check_root

## usercheck
echo "Your user is:"
user=$USER
echo -n "${c1}"
echo $user
echo -n "${n}"
echo

## case question
echo -n "${c2}choices:${n}"
echo "0 - ***changme***"
echo "1 - ***changme***"
echo "2 - ***changme***"
echo "3 - ***changme***"
echo "4 - ***changme***"
echo "5 - ***changme***"
echo "q - quit"
echo -n "${c2}"
read -p "posible answers: (y/N) " ***changme***
echo -n "${n}"
case $***changme*** in
	y )
	***changme***
	echo -n "${c2}Done${n}"
	;;

	n )
	;;
esac

### another case
	5 )
	echo -n "${c2}***changme***${n}"
	echo -n "${c2}Done${n}"
	;;

### not here yet
echo "***changme***"

### red 
echo -n "${c1}***changme***${n}"

### NOT HERE YET
echo "***changme***"
### BLUE
echo -n "$c4}***changme***${n}"
### GREEN 
echo -n "$c2}***changme***${n}"
### RED 
echo -n "$c1}***changme***${n}"
### DONE
echo -n "${c2}Done${n}"
