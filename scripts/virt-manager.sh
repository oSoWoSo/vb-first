#!/bin/bash

#
# Install and setup KVM/QEMU/Virt-Manager with file sharing
#
# Copyright (c) 2022 zenobit from oSoWoSo
# mail: <zen@osowoso.xyz> web: https://osowoso.xyz
# licensed under EUPL 1.2
# sources:
# dt's Virt-Manager Is The Better Way To Manage VMs
# https://www.youtube.com/watch?v=p1d_b_91YlU
# dt's Virt-Manager Tips and Tricks from a VM Junkie
# https://www.youtube.com/watch?v=9FBhcOnCxM8&t=1046s
#

## colors
blue=$(tput setaf 4)
green=$(tput setaf 2)
red=$(tput setaf 1)
none=$(tput sgr0)
echo -n "$green"
echo "choices:"
echo -n "$none"
echo "0 - install required"
echo "1 - share on host"
echo "2 - share on guest"
echo "3 - convert image"
echo "4 - change resolution (one monitor only)"
echo "5 - guest addition"
echo "q - quit"
echo -n "$green"
read -p "posible answers: (0/1/2/3/4/q)" whattodo
echo -n "$none"
case $whattodo in
	q )
	echo -n "$red"
	echo "quit"
	echo -n "$none"
	exit
	;;

	0 )
	## usercheck
	echo "Your user is:"
	user=$USER
	echo -n "$red"
	echo $user
	echo -n "$none"
	echo
	## check virtualization support
	echo -n "$green"
	echo check virtualization support
	echo -n "$none"
	LC_ALL=C lscpu | grep Virtualization
	## install needed
	echo -n "$green"
	echo install needed
	echo -n "$none"
	sudo xbps-install\
	 dbus\
	 libvirt\
	 qemu\
	 virt-manager\
	 bridge-utils\
	 iptables
	modules-load
	
	## add service
	echo -n "$green"
	echo add service
	echo -n "$none"
	sudo ln -s /etc/sv/dbus /var/service/
	sudo ln -s /etc/sv/libvirtd /var/service/
	sudo ln -s /etc/sv/virtlockd /var/service/
	sudo ln -s /etc/sv/virtlogd /var/service/
	## add user to libvirt group
	echo -n "$green"
	echo add user to libvirt group
	echo -n "$none"
	sudo usermod -G libvirt -a $user
	echo -n "$green"
	echo "Done"
	echo -n "$none"
	;;

	1 )
	## create shared folder betwen host and guest
	mkdir ~/share
	## give permission to anyone
	chmod 777 ~/share
	echo -n "$green"
	echo "Done"
	echo -n "$none"
	;;

	2 )
	## create shared folder betwen host and guest
	## add new filesystem
	## type: mount 
	## mode: mapped
	## source path: /home/$USER/share
	## target path: /sharepoint
	mkdir ~/share
	## always mount shared directory
	sudo mount -t 9p -o trans=virtio /sharepoint share
	## or
	## auto mount at start 
	## add to /etc/fstab "/sharepoint	/home/$USER/share	9p	trans=virtio,version=9p2000.L,rw	0	0"
	echo -n "$green"
	echo "Done"
	echo -n "$none"
	;;

	3 )
	### Convert images: virtualbox vdi to gcow2
	echo "***not here yet***"
	#sudo qemu-img convert -f vdi -O qcow2 Ubuntu\ 20.04.vdi /var/lib/libvirt/images/ubuntu-20-04.qcow2
	echo -n "$green"
	echo "Done"
	echo -n "$none"
	;;

	4 )
	echo "choose:"
	echo "a - fullHD"
	echo "b - custom"
	read -p "posible answers: (a/b)" resolution
	case $resolution in
		a )
		## set fullHD
		xrandr -s 1920x1080
		echo -n "$green"
		echo "Done"
		echo -n "$none"
		;;

		b )
		## custom resolution
		echo "input custom resolution"
		echo "example: 1920x1080"
		read -p "custom" custom
		xrandr -s $custom
		echo -n "$green"
		echo "Done"
		echo -n "$none"
		;;
	esac
	;;

	5 )
	echo -n "$red"
	echo $user
	echo -n "$none"
	
	echo -n "$green"
	echo "Done"
	echo -n "$none"
	;;
esac

echo -n "$red"
echo "Finished"
echo -n "$none"
