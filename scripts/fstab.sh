#!/usr/bin/bash

#
# fstab script for vb aka VoidGNU/Linux
#
# Copyright (c) 2022 zenobit from oSoWoSo
# mail: <zen@osowoso.xyz> web: https://osowoso.xyz
# licensed under EUPL 1.2
# source:
# 

echo "list all disks by uuid"
echo
ls -al /dev/disk/by-uuid/

echo"your current fstab"
echo
cat /etc/fstab

echo "xxx"

echo "check errors in fstab"
echo
sudo findmnt --verify

echo done

# example:
#	# /etc/fstab: static file system information.
#	#
#	# Use 'blkid' to print the universally unique identifier for a
#	# device; this may be used with UUID= as a more robust way to name devices
#	# that works even if disks are added and removed. See fstab(5).
#	#
#	# <file system> <mount point>   <type>  <options>       <dump>  <pass>
#	# / was on /dev/sdb1 during installation
#	UUID=63a46dce-b895-4c1f-9034-b1104694a956 /               ext4    errors=remount-ro 0       1
#	# swap was on /dev/sdb5 during installation
#	UUID=b9b9ee49-c69c-475b-894b-1279d44034ae none            swap    sw              0       0
#	# data drive
#	UUID=19fa40a3-fd17-412f-9063-a29ca0e75f93 /media/data   ext4    defaults        0       0
