#!/usr/bin/bash

#
# hugo install script for vb aka VoidGNU/Linux
#
# Copyright (c) 2022 zenobit from oSoWoSo
# mail: <zen@osowoso.xyz> web: https://osowoso.xyz
# licensed under EUPL 1.2
# source:
# 

sudo xbps-install\
 ansible\
 certbot-nginx\
 hugo\
 nginx

mkdir $projects-dir
cd $projects-dir
hugo new site $site
cd $site
git init

echo done
