#!/usr/bin/bash

#
# zensu install script for vb aka VoidGNU/Linux
#
# Copyright (c) 2022 zenobit from oSoWoSo
# mail: <zen@osowoso.xyz> web: https://osowoso.xyz
# licensed under EUPL 1.2
# source:
#

git clone https://github.com/oSoWoSo/zensu
cd zensu
sudo make install
echo done
