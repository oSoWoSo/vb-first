#!/usr/bin/bash

#
# ngetty install script for vb aka VoidGNU/Linux
#
# Copyright (c) 2022 zenobit from oSoWoSo
# mail: <zen@osowoso.xyz> web: https://osowoso.xyz
# licensed under EUPL 1.2
# source:
# 

sudo xbps-install\
 ngetty

sudo rm -vf /var/service/agetty* &&\
 sudo ln -s /etc/sv/ngetty /var/service

echo done
