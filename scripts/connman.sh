#!/usr/bin/bash

#
# connman install script for vb aka VoidGNU/Linux
#
# Copyright (c) 2022 zenobit from oSoWoSo
# mail: <zen@osowoso.xyz> web: https://osowoso.xyz
# licensed under EUPL 1.2
# source:
# 

sudo xbps-remove\
 libnma\
 NetworkManager\
 network-manager-applet

sudo xbps-install\
 connman\
 connman-ui

sudo rm -vf /var/service/dhcpcd &&\
 sudo ln -s /etc/sv/connmand /var/service/

echo done
