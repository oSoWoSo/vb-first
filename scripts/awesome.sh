#!/usr/bin/bash

#
# awesomewm install script for vb aka VoidGNU/Linux
#
# Copyright (c) 2022 zenobit from oSoWoSo
# mail: <zen@osowoso.xyz> web: https://osowoso.xyz
# licensed under EUPL 1.2
# source:
# 

sudo xbps-install\
 awesome

cp -r /etc/xdg/awesome ~/.config/awesome
echo done
