#!/usr/bin/bash

#
# install script for gaming on vb aka VoidGNU/Linux
#
# Copyright (c) 2022 zenobit from oSoWoSo
# mail: <zen@osowoso.xyz> web: https://osowoso.xyz
# licensed under EUPL 1.2
# source:
# 

## colors
blue=$(tput setaf 4)
green=$(tput setaf 2)
red=$(tput setaf 1)
none=$(tput sgr0)

# STEAM
sudo xbps-install -f\
 alsa-plugins-{pulseaudio,ffmpeg}-32bit\
 alsa-sndio\
 alsa-tools-32bit\
 apulse-32bit\
 bluez-alsa-32bit\
 mesa-opencl\
 mesa-{dri,vaapi,opencl}-32bit\
 mono\
 sndio\
 steam\
 {vulkan-loader\
,libgcc\
,libstdc++\
,libdrm\
,libglvnd}-32bit\
 vulkan-loader\
 vkd3d

# LUTRIS                                                                   #
sudo xbps-install -Sfy\
 gst-plugins-{bad1,good1,ugly1}\
 gstreamer-vaapi\
 libgstreamerd\
 lutris\
 mpg123\
 pulseaudio-module-sndio\
 {alsa-plugins\
,alsa-sndio\
,gtk+3\
,giflib\
,gnutls\
,gst-plugins-bad1\
,gstreamer-vaapi\
,libgcrypt\
,libgpg-error\
,libjpeg-turbo\
,libldap\
,libopenal\
,libpng\
,sndio\
,sqlite\
,v4l-utils\
,libXcomposite\
,libXinerama}-32bit

# Rather than worry about modifying `limits.conf` per-user, this will also #
# create a group and set that group's limits.  The user script will add    #
# the user to this group.  The only other, sensible alternative was adding #
# `wheel` to this conf.                                                    #
# Create ulimit524288 group and increase open files limit
sudo groupadd ulimit524288 &&
sudo sed -ie 48'a \@ulimit524288\thard\tnofile\t524288' /etc/security/limits.conf
usermod -a -G ulimit524288

# WINE
sudo xbps-install -f\
 wine\
 winetricks\
 wine-{common\
,devel\
,gecko\
,mono\
,tools}-32bit

echo done
