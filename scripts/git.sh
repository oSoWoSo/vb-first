#!/usr/bin/bash

#
# git install script for vb aka VoidGNU/Linux
#
# Copyright (c) 2022 zenobit from oSoWoSo
# mail: <zen@osowoso.xyz> web: https://osowoso.xyz
# licensed under EUPL 1.2
# sources:
#

sudo xbps-install\
 git

git config --global user.email "<EMAIL>"
git config --global user.name "<GITNAME>"
git config --global credential.helper store
git secret init

echo done
