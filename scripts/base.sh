#!/usr/bin/bash

#
# base install script for vb aka VoidGNU/Linux
#
# Copyright (c) 2022 zenobit from oSoWoSo
# mail: <zen@osowoso.xyz> web: https://osowoso.xyz
# licensed under EUPL 1.2
# source:
# 

sudo xbps-install\
 dbus\
 dosfstools\
 elogind\
 gnome-keyring\
 gnome-ssh-askpass\
 mtools\
 polkit\
 xtools 

echo done
