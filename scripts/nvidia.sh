#!/usr/bin/bash

#
# nvidia install script for vb aka VoidGNU/Linux
#
# Copyright (c) 2022 zenobit from oSoWoSo
# mail: <zen@osowoso.xyz> web: https://osowoso.xyz
# licensed under EUPL 1.2
# source:
# 

sudo xbps-install\
 dkms\
 nvidia\
 nvtop\
 pkg-config\
 nvidia-libs-32bit

echo done
