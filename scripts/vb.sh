#!/usr/bin/bash

# ------------------------------------------
# Install script for vb aka VoidGNU/Linux
#       and excellent Void Linux
# intended to be runned from live cd...
# builded for Ryzen 5 2600 Nvidia 1050 Ti
# ------------------------------------------
# Copyright (c) 2022 zenobit from oSoWoSo
# mail: <zen@osowoso.xyz> web: https://osowoso.xyz
# licensed under MIT license
# sources and inspirations:
#  https://github.com/netzverweigerer/vpm
#  https://www.reddit.com/r/voidlinux/comments/qsyk32/how_to_connect_to_the_fastest_mirrors_in_void
#  https://github.com/void-linux/void-mklive
#  https://github.com/box-supremacy/void-installer

# functions + template INFO
# all vb templates could contain "___changme___"
# find it all and CHANGE IT!
# Not here yet! not yet been implemented..

# change me
___changme___() {
	echo "Not here yet!" #___changme___
	
}

# colors
Colors() {
	c0=$(tput setaf 0) # black
	c1=$(tput setaf 1) # red
	c2=$(tput setaf 2) # green
	c3=$(tput setaf 3) # yellow
	c4=$(tput setaf 4) # blue
	c5=$(tput setaf 5) # 
	c6=$(tput setaf 6) # light blue
	c7=$(tput setaf 7) # white
	c8=$(tput setaf 8) # grey
	c9=$(tput setaf 9) # 
	# reset
	c=$(tput sgr0)
	# backgrounds
	b0=$(tput setab 0)
	b1=$(tput setab 1)
	b2=$(tput setab 2)
	b3=$(tput setab 3)
	b4=$(tput setab 4)
	b5=$(tput setab 5)
	b6=$(tput setab 6)
	b7=$(tput setab 7)
	b8=$(tput setab 8)
	b9=$(tput setab 9)
}
# Help usage
Help() {
	: "${progname:="${0##*/}"}"
	cat <<_EOF | GREP_COLORS='ms=1' egrep --color "$progname|$"
----------------------------------------------------------
Install script for vb aka VoidGNU/Linux
             and excellent Void Linux
intended to be runned from live cd
builded for (Ryzen 5 2600 Nvidia 1050 Ti)
----------------------------------------------------------
       pkg path: ${PKG_PATH}
       test speed limit: $(echo $SPEED_LIMIT | numfmt --to=iec-i --suffix=B/s)
       mirrors for testing: $(echo "$MIRRORLIST" | wc -l)
----------------------------------------------------------
Usage: $progname -h                 show this help an exit
----------------------------------------------------------
       $progname -f file.csv        format speedtest results from file and pass to fzf
       $progname -i                 install vb to drive
       $progname -m                 manually set mirror
       $progname -w [file.csv]      save speedtest results to file
       $progname -r file.csv        format speedtest results from file
----------------------------------------------------------
            Help me make it better please!
----------------------------------------------------------
_EOF
}
# rootcheck
check_root() {
	if [[ $EUID -gt 0 ]]; then
		echo -e "\n${c1}This operation needs super-user privileges."
		echo -e "${c2}exiting...${n}"
		exit 255
	fi
}
# detect if this is an EFI system.
check_efi() {
	if [ -e /sys/firmware/efi/systab ]; then
		EFI_SYSTEM=1
		echo -e "\n${c2} EFI system enabled ${n}"
		EFI_FW_BITS=$(cat /sys/firmware/efi/fw_platform_size)
		if [ $EFI_FW_BITS -eq 32 ]; then
			EFI_TARGET=i386-efi
		else
			EFI_TARGET=x86_64-efi
		fi
	else
		echo -e "\n${c1} no EFI system support ${n}\n"
		echo -e "${c2}exiting...${n}"
		exit 1
	fi
}
# format drives
format() {
	echo -e "\n${c4}Formating partitions${c1}...${n}"
	mkfs.vfat /dev/${boot}
	echo -e "${c2}done${n}"
	mkfs.btrfs -f /dev/${root}
	echo -e "${c2}done${n}"
}
# install vb to drive
install() {
echo -e "\n${c2}-${c3}-${c1}- ${c2}Let's ${c3}begin ${c1}installing ${c2}-${c3}-${c1}-${n}"
# run functions
install_required
#check_root
check_efi
umount_all

echo -e "${c4}Mounting cache${c1}...${n}"
sudo mount /dev/nvme0n1p4 /var/cache/

echo -e "\n${c1}Required${n} two partitions (${c1}boot${n} and ${c1}root${n})"
echo -e "- ${c3}recommended${n} separate home partition"
echo -e "- ${c3}optional${n} separate cache partition"

use_gparted
sleep 1
next
echo -e "${c1}Drives list:${n}"
sudo fdisk -x | grep "/dev/"

set_boot
set_root
set_home
set_cache
format
mount_target
install_method

echo -e "\n${c2}Entering chroot${n}"
echo -e "${c4}Remounting cache${c1}...${n}"
sudo cp -r /mnt/target/var/cache/ /var/
sudo umount /dev/${cache}
sudo mount /dev/${cache} /mnt/target/var/cache/

mount_chroot

echo -e "${c4}Copying the DNS configuration${c1}...${n}"
sudo cp /etc/resolv.conf /mnt/target/etc/

echo -e "${c4}Copying setted mirrors${c1}...${n}"
sudo cp -r /etc/xbps.d /mnt/target/etc/

echo -e "${c4}Copying installation script to chroot${c1}...${n}"
curl http://10.0.1.3:3000/oSoWoSo/vb-scripts/raw/branch/master/vb2.sh -o vb2.sh
chmod +x vb2.sh
sudo cp vb2.sh /mnt/target

echo -e "\n${c2}Chroot into the new installation${n}"
PS1='(chroot) # ' sudo chroot /mnt/target/ /bin/bash  "./vb2.sh"
}
# install awesome WM
install_awesome() {
	echo "Not here yet!" #___changme___
	
}

install_method () {
	echo -e "\n${c2}Choose installation method...${n}"
	echo -e "Possible (type in number):
	- 1 rootfs
	- 2 xbps ${c3}recommended${n}
	- 3 cd copy"
	read -p "" method
	case $method in
		1 )
			xbps-install -y xz
			echo -e "${c4}Downloading void linux ROOTFS tarball${c1}...${n}"
			## Detect if ROOTFS already downloaded.
			if [ -e ./void-ROOTFS.tar.xz ]; then
				tar xvf void-ROOTFS.tar.xz -C /mnt/target
			else
				curl https://mirror.fit.cvut.cz/voidlinux/live/current/void-x86_64-ROOTFS-20210930.tar.xz -o void-ROOTFS.tar.xz
				tar xvf void-ROOTFS.tar.xz -C /mnt/target
			fi
		;;
	
		2 )
			REPO=${newrepo}/current
			ARCH=x86_64
			echo -e "${c4}Installing base system${c1}...${n}"
			XBPS_ARCH=$ARCH xbps-install -Sy -r /mnt/target -R "$REPO" base-system fish-shell refind
			;;
		3 )
			echo "Not implemented yet!"
		;;
	esac
}
# install openbox WM
install_openbox() {
	echo "Not here yet!" #___changme___
	
}
# required dependencies
install_required() {
	MIRRORLIST="$(wget -q -O- "https://raw.githubusercontent.com/void-linux/void-docs/master/src/xbps/repositories/mirrors/index.md" | grep "<http" |cut -d'<' -f2 |cut -d'>' -f1)"
	sudo xbps-install -y xtools gparted fzf mtools wget
}
# LVM
lvm() {
	echo "Not here yet!" # CHANGE IT!
	
}

mirror_speed() {
	curl -Y $SPEED_LIMIT --progress-bar $1 -o/dev/null --write-out "%{speed_download}"
}
# test mirrors
mirror_test() {
	count=1
	echo "$MIRRORLIST" | while read -r mirror etc; do
		pkg_url="${mirror}${PKG_PATH}"
		info=$(echo $etc | sed 's/,//g')
		>&2 echo "${count}. $pkg_url"
		echo "${mirror},${info},$(time_appconnect $pkg_url),$(mirror_speed $pkg_url)"
		>&2 echo
		count=$((count + 1))
	done
}
# sort mirrors test results
mirror_test_sort_results() {
	cat "$1" | sort -t, -nrk4 | while IFS=, read url info time_appconnect mirror_speed; do
		f_time=$(printf "%.2fs" $time_appconnect)
		f_speed=$(echo $mirror_speed | numfmt --to=iec-i --suffix=B/s)
		echo "${url},${info},${f_time},${f_speed}"
	done | column -s, -t
}
# mount chroot
mount_chroot() {
	sudo mount --rbind /sys /mnt/target/sys && sudo mount --make-rslave /mnt/target/sys
	sudo mount --rbind /dev /mnt/target/dev && sudo mount --make-rslave /mnt/target/dev
	sudo mount --rbind /proc /mnt/target/proc && sudo mount --make-rslave /mnt/target/proc
}
# mount target
mount_target() {
	echo -e "${c4}Mounting partitions${c1}...${n}"
	sudo mkdir -p /mnt/target
	sudo mount /dev/${root} /mnt/target/
	sudo mkdir -p /mnt/target/boot
	sudo mount /dev/${boot} /mnt/target/boot
	sudo mkdir -p /mnt/target/home
	sudo mount /dev/${home} /mnt/target/home/
	sudo mkdir -p /mnt/target/var/cache
	sudo mount /dev/${cache} /mnt/target/var/cache/
}
# continue installing
next() {
	echo "Wanna continue? y/N"
	read -p "" gonext
	case $gonext in
	y) ;;
	
	*) exit 1 ;;
	
	esac
}
# set boot partition
set_boot() {
	echo -e "${c2}Enter partition for boot: ${c1}required${n}"
	echo -e "(${c4}blue${n} = personal defaults)"
	echo -e "(sda1 ${c4}sdb1${n} vda1 nvme0n1p1${c1}...${n})"
	read -p "" boot
	echo -e "${c4}root will be on ${c1}${boot}${n}"
	export boot
}
# set cache partition
set_cache() {
	echo -e "\n${c2}Enter partition for cache: ${c3}optional${n}"
	echo -e "(sda4 sdb4 vda4 ${c4}nvme0n1p4${n}${c1}...${n})"
	read -p "" cache
	echo -e "${c4}root will be on ${c1}${cache}${n}"
	export cache
}
# set home partition
set_home() {
	echo -e "\n${c2}Enter partition for home: ${c3}recommended${n}"
	echo -e "(sda3 sdb3 vda3 ${c4}nvme0n1p3${n}${c1}...${n})"
	read -p "" home
	echo -e "${c4}root will be on ${c1}${home}${n}"
	export home
}
# set new mirror
set_mirror() {
	echo -e "${c2}Changing mirrors...${n}"
	defaultrepo="https://alpha.de.repo.voidlinux.org"
	echo "default repository: ${defaultrepo}"
	newrepo="https://mirror.fit.cvut.cz/voidlinux"
	echo "new repository: ${newrepo}"
	sudo mkdir -p /etc/xbps.d
	sudo cp /usr/share/xbps.d/*-repository-*.conf /etc/xbps.d/
	sudo sed -i "s|https://alpha.de.repo.voidlinux.org|$newrepo|g" /etc/xbps.d/*-repository-*.conf
	echo -e "${c4}Synchronizing${c1}...${n}"
	xbps-install -Suvy
}
# set root partition
set_root() {
	echo -e "\n${c2}Enter partition for root: ${c1}required${n}"
	echo -e "(sda2 ${c4}sdb2${n} vda2 nvme0n1p2${c1}...${n})"
	read -p "" root
	echo -e "${c4}root will be on ${c1}${root}${n}"
	export root
}

time_appconnect() {
	curl --connect-timeout 2 -o/dev/null -sw '%{time_appconnect}' -I $1
}
# unmount all partitions
umount_all() {
	echo -e "\n${c4}Unmounting all partitions${c1}...${n}"
	sudo umount /dev/sda1
	sudo umount /dev/sda2
	sudo umount /dev/sda3
	sudo umount /dev/sda4
	sudo umount /dev/sdb1
	sudo umount /dev/sdb2
	sudo umount /dev/sdb3
	sudo umount /dev/sdb4
	sudo umount /dev/vda1
	sudo umount /dev/vda2
	sudo umount /dev/vda3
	sudo umount /dev/vda4
	sudo umount /dev/vdb1
	sudo umount /dev/vdb2
	sudo umount /dev/vdb3
	sudo umount /dev/vdb4
	sudo umount /dev/nvme0n1p1
	sudo umount /dev/nvme0n1p2
	sudo umount /dev/nvme0n1p3
	sudo umount /dev/nvme0n1p4
}
# update system
update() {
	sudo xbps-install -Sy xbps
	sudo xbps-install -Syu
	xcheckrestart
}
# prepare drives for installation
use_gparted() {
	echo -e "${c2}Use gparted...?${n}" && read gpart
	case $gpart in
		y )
			sudo gparted &
		;;
		* )
		;;
	esac
}
# exit
quit() {
	echo "----------------------------------------------------------"
	echo "       Enjoy and help me make it better please!"
	echo "       https://osowoso.xyx     <pm@osowoso.xyz>"
	echo "----------------------------------------------------------"
	exit 0
}
## RUN
Colors
case $1 in
	-f) mirror_test_sort_results $2 | fzf +s;;
	-i) echo "installing vb to drive" && install;;
	-m) echo "Choose mirror:" && set_mirror;;
	-r) mirror_test_sort_results $2;;
	-w) [ -z $2 ] && { mirror_test; exit 0; } || { mirror_test >"$2"; exit 0; };;
	*) Help;;
esac

PKG_PATH="current/glibc-2.32_2.x86_64.xbps"
SPEED_LIMIT="1048576"

# Exit immediately if a command exits with a non-zero exit status
#set -e

quit
