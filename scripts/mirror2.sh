#!/usr/bin/bash

# Choose best mirror
## COLORS
c1=$(tput setaf 1) # red
c2=$(tput setaf 2) # green
n=$(tput sgr0)

DEFAULT="https://alpha.de.repo.voidlinux.org"
MIRRORLIST=$(wget -q -O- "https://raw.githubusercontent.com/void-linux/void-docs/master/src/xbps/repositories/mirrors/index.md" | grep "<http" |cut -d'<' -f2|cut -d'>' -f1 > mirror.list)
old="https://alpha.de.repo.voidlinux.org"
echo "Current mirror is: ${c2}${old}${n}"
nl mirror.list > mirrors
echo ${c1}"Mirror list:"${n}

cat mirror.list |cut -d'/' -f3 > ping
fping -f ping

cat mirrors
read -p "Enter number of new mirror:" number
new=$(cat mirror.list | awk "NR == ${number}")
echo "Your new mirror is: ${c2}${new}${n}"

sudo cp /usr/share/xbps.d/*-repository-*.conf /home/zen/All-projects/vb-scripts/scripts/test
sudo sed -i "s|${old}|${new}|g" /home/zen/All-projects/vb-scripts/scripts/test/*-repository-*.conf
#sudo mkdir -p /etc/xbps.d
#sudo cp /usr/share/xbps.d/*-repository-*.conf /etc/xbps.d
#sudo sed -i "s|${old}|${new}|g" /etc/xbps.d/*-repository-*.conf
