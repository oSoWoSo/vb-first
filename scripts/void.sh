#!/bin/sh

#
# void postinstall
# Copyright (c) 2022 zenobit from oSoWoSo
# mail: <zen@osowoso.xyz> web: https://osowoso.xyz
# licensed under EUPL 1.2
# sources: https://git.ipv6.sk/Zezik/void/src/branch/master/void.sh
#

# repo
sudo xbps-install -Su\
 void-repo-multilib\
 void-repo-multilib-nonfree\
 void-repo-nonfree

# balicky
# adwaita-icon-theme - ikony
# bash-completion - doplnovani prikazu
# bc - kalkulacka
# chromium - web browser
# dmenu - pousteni programu
# engrampa - spravce archivu
# firefox - web browser
# git - git
# google-fonts-ttf - fonty
# htop - prohlizec procesu
# i3 - window manager
# libglvnd-32bit - potreba pro steam
# libreoffice - kancelarskej balik
# lutris - herni klient
# lxterminal - terminal
# micro - text editor
# mousepad - GUI text editor
# mpv - videoprehravac
# MultiMC - minecraft klient
# neofetch - info o systemu
# newsboat - RSS ctecka
# nitrogen - nastavovani wallpaperu
# nvidia - ovladac graficke karty
# nvidia-libs-32bit - potreba pro steam
# obs - nahravani plochy
# pcmanfm - souborovej manazer
# pulseaudio - zvuk
# pulsemixer - ovladani zvuku
# remmina - RDP klient
# ristretto - prohlizec obrazku
# steam - hry
# thunderbird - email klient
# transmission-gtk - torrenty
# unzip - rozbalovani zipu
# vba-m - GBA emulator
# virtualbox-ose - vbox
# vulkan-loader-32bit # potreba pro wine
# wget - stahovani souboru
# wine-32bit - spousteni exe
# wine-gecko - potreba pro wine
# wine-mono - potreba pro wine
# winetricks - potreba pro wine
# xclip - schranka
# xfce4-screenshooter - porizovani screenshotu
# xorg - GUI
# youtube-dl - stahovani youtube videi
# zathura-pdf-poppler - PDF ctecka
sudo xbps-install -Su\
 adwaita-icon-theme\
 bash-completion\
 bc\
 chromium\
 dmenu\
 engrampa\
 firefox\
 git\
 google-fonts-ttf\
 htop\
 i3\
 libglvnd-32bit\
 libreoffice\
 lutris\
 lxterminal\
 micro\
 mousepad\
 mpv\
 MultiMC\
 neofetch\
 newsboat\
 nitrogen\
 nvidia\
 nvidia-libs-32bit\
 obs\
 pcmanfm\
 pulseaudio\
 pulsemixer\
 remmina\
 ristretto\
 steam\
 thunderbird\
 transmission-gtk\
 unzip\
 vba-m\
 virtualbox-ose\
 vulkan-loader-32bit\
 wget\
 wine-32bit\
 wine-gecko\
 wine-mono\
 winetricks\
 xclip\
 xfce4-screenshooter\
 xorg\
 youtube-dl\
 zathura-pdf-poppler\

# nastaveni
sudo ln -s\
 /usr/share/fontconfig/conf.avail/70-no-bitmaps.conf\
 /etc/fonts/conf.d/

echo "setxkbmap -layout cz & nitrogen --restore & exec i3"\
 > /home/zezik/.xinitrc

# reboot
sudo reboot


