#!/usr/bin/bash

#
# xbps xtraeme install script for vb aka VoidGNU/Linux
#
# Copyright (c) 2022 zenobit from oSoWoSo
# mail: <zen@osowoso.xyz> web: https://osowoso.xyz
# licensed under EUPL 1.2
# source:
# 

cd
sudo xbps-install\
 gcc\
 git\
 make\
 pkg-config\
 zlib\
 zlib-devel\
 openssl\
 libarchive\
 libarchive-devel

mkdir xbps
curl https://gitlab.com/xtraeme/xbps/-/archive/21.1/xbps-21.1.tar -o xbps-21.1.tar
cd xbps
tar -xf xbps-21.1.tar
./configure --prefix=/usr --sysconfdir=/etc
make -j$(nproc)
make DESTDIR=~/xbps-git install clean
export PATH=~/xbps-git/usr/bin:$PATH
sudo link /etc/ssl/certs/ca-certificates.crt /etc/ssl/cert.pem
xbps-query -V
echo done
