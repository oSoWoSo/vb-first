#!/usr/bin/bash

#
# terminal apps install script for vb aka VoidGNU/Linux
#
# Copyright (c) 2022 zenobit from oSoWoSo
# mail: <zen@osowoso.xyz> web: https://osowoso.xyz
# licensed under EUPL 1.2
# source:
# 

sudo xbps-install\
 bpytop\
 fish-shell\
 fuzzypkg\
 neofetch\
 topgrade\
 vpsm\
 vsv

echo done
