#!/usr/bin/bash

#
# add user script and groups vb aka VoidGNU/Linux
#
# Copyright (c) 2022 zenobit from oSoWoSo
# mail: <zen@osowoso.xyz> web: https://osowoso.xyz
# licensed under EUPL 1.2
# source:
# 

echo $SHELL

# ===MAKE USER===
printf "[1;4mPlease input Username[0;1m:[0m"
printf "(Please keep UNIX-compliant): "

read MY_USERNAME

# Rather than worry about modifying `limits.conf` per-user, this will also #
# create a group and set that group's limits.  The user script will add    #
# the user to this group.  The only other, sensible alternative was adding #
# `wheel` to this conf.                                                    #
# Create ulimit524288 group and increase open files limit
groupadd $USER _ulimit &&
sed -ie 48'a \@_ulimit\thard\tnofile\t524288' /etc/security/limits.conf

useradd -Um -s $SHELL -G\
 audio\
,bluetooth\
,disk\
,_ulimit\
,network\
,storage\
,video\
,wheel\
 "$MY_USERNAME"

printf "\nUser \'%s\' created\n" "$MY_USERNAME"

echo done
