#!/usr/bin/bash

#
# gitea install script for vb aka VoidGNU/Linux
#
# Copyright (c) 2022 zenobit from oSoWoSo
# mail: <zen@osowoso.xyz> web: https://osowoso.xyz
# licensed under EUPL 1.2
# sources:
#

# install build dependencies
sudo xbps-install\
 gcc\
 git\
 go\
 nodejs\
 make\
 pnpm

echo "choose version to install:"
echo "1: by branch"
echo "2: by tag"
read -p "posible answers: (1/2)" gitea
case $gitea in
	1 )
	git branch -a
	echo -n "$green"
	echo "Done"
	echo -n "$none"
	;;

	2 )
	git tag -l
	echo
	echo "insert version to install"
	read -p "and press Enter" git
	git checkout $git
	echo -n "$green"
	echo "Done"
	echo -n "$none"
	;;
esac

# clone gitea
git clone https://github.com/go-gitea/gitea ~/all-projects/gitea
# OR
#git clone https://codeberg.org/Codeberg/gitea ~/all-projects/gitea
cd ~/all-projects/gitea

# build
TAGS="bindata sqlite sqlite_unlock_notify" make build
./gitea web

# create gaming group
groupadd -f gaming

# create user git
useradd -Um -s /bin/fish -G gaming,_gitea git &&
sed -ie 48'a \@gaming\thard\tnofile\t524288' /etc/security/limits.conf
# increase open files limit

# run gitea
gitea --verbose -C ~/data/gitea -c ~/data/gitea/gitea.ini  -w ~/data/gitea

# then open browser on http://0.0.0.0:3000 and setup...
# database type: sqlite3
# path: $USER/data/gitea/gitea.db
# title: Gitea by oSoWoSo: open Source World Society
# repository root path: $HOME/data/gitea-production/data/gitea-repositories
# git lfs root path: $HOME/data/gitea-production/data/lfs
# run as user: zen
# SSH server domain: localhost
# SSH server port: 22
# gitea HTTP listen port: 3000
# gitea base url: https://git.osowoso.xyz:3000/
# log path: $HOME/data/gitea-production/log
# Optional:
# SMTP host: disroot.org
# send mail as: oSoWoSo <zen@osowoso.xyz>
# SMTP username: zenobit
# SMTP password: 
# require email confirmation to register
# enable email norifications
# enable federated avatars
# hide email adresses by default
# allow creation of organizations by default
# hidden email domain: noreply.osowoso.xyz
# password hash algorithm: pdkdf2
# administrator user name: zenobit
# password: masakr123
# email address: zen@osowoso.xyz
