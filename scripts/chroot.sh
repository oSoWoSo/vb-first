#!/usr/bin/bash

#
# chroot script for vb aka VoidGNU/Linux
# intended to be runned on live cd or anywhere else
#
# Copyright (c) 2022 zenobit from oSoWoSo
# mail: <zen@osowoso.xyz> web: https://osowoso.xyz
# licensed under EUPL 1.2
# 

## COLORS
b=$(tput setaf 4)
g=$(tput setaf 2)
r=$(tput setaf 1)
y=$(tput setaf )
w=$(tput setaf )
# no color
n=$(tput sgr0)

## rootcheck
rootcheck () {
  if [[ $EUID -gt 0 ]]; then
    echo -e "\n${r}This operation needs super-user privileges.${n}\n"
    exit 255
  fi
}
rootcheck
## 
#mount --rbind /sys /mnt/sys && mount --make-rslave /mnt/sys
#mount --rbind /dev /mnt/dev && mount --make-rslave /mnt/dev
#mount --rbind /proc /mnt/proc && mount --make-rslave /mnt/proc
#cp /etc/resolv.conf /mnt/etc/
#PS1='(chroot) # ' chroot /mnt/ /bin/bash
echo -n "$g"
echo "if your installation failed and you still have mounted new system...${n}"
echo "(You not click on < OK > button yet)"
echo -n
mount --rbind /sys /mnt/target/sys && mount --make-rslave /mnt/target/sys
mount --rbind /dev /mnt/target/dev && mount --make-rslave /mnt/target/dev
mount --rbind /proc /mnt/target/proc && mount --make-rslave /mnt/target/proc
cp /etc/resolv.conf /mnt/target/etc/
PS1='(chroot) # ' chroot /mnt/target/ /bin/bash
