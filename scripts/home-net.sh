#!/usr/bin/bash

#
# home network install script for vb aka VoidGNU/Linux
#
# Copyright (c) 2022 zenobit from oSoWoSo
# mail: <zen@osowoso.xyz> web: https://osowoso.xyz
# licensed under EUPL 1.2
# sources:
#
# COLORS
b=$(tput setaf 4)
g=$(tput setaf 2)
r=$(tput setaf 1)
none=$(tput sgr0)

echo -n "$g"
echo "done"
echo -n "$none"
