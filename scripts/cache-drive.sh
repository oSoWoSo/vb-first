#!/usr/bin/bash
#  install script for vb aka VoidGNU/Linux
# Copyright (c) 2021 oSoWoSo <zen@osowoso.xyz>
# https://oSoWoSo.xyz
# licensed under EUPL 1.2
# sources:
# ***CHANGME***
# COLORS
b=$(tput setaf 4)
g=$(tput setaf 2)
r=$(tput setaf 1)
y=$(tput setaf )
w=$(tput setaf )
# no color
n=$(tput sgr0)
## QUESTION AND ANSWER
printf "\n[1;4mPlease input [0;1m:[0m\n"
printf "(Enter partition for cache): "
read CACHE
echo $CACHE
sudo umount /dev/$CACHE
sudo mount /dev/$CACHE /var/cache/

echo -n "$green"
echo "Done"
echo -n "$none"
