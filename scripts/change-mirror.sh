#!/usr/bin/bash

#
# mirror change script for vb aka VoidGNU/Linux
#
# Copyright (c) 2022 zenobit from oSoWoSo
# mail: <zen@osowoso.xyz> web: https://osowoso.xyz
# licensed under EUPL 1.2
# source:
# 

# main: https://alpha.de.repo.voidlinux.org
# my choice: repository=https://mirror.fit.cvut.cz/voidlinux
repository=https://mirror.fit.cvut.cz/voidlinux
echo $repository

sudo mkdir -p /etc/xbps.d
sudo cp /usr/share/xbps.d/*-repository-*.conf\
 /etc/xbps.d/

sudo sed -i 's|https://alpha.de.repo.voidlinux.org|https://mirror.fit.cvut.cz/voidlinux|g' /etc/xbps.d/*-repository-*.conf
sudo xbps-install -S

echo done
