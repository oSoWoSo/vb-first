#!/usr/bin/bash

#
# shell install script for vb aka VoidGNU/Linux
#
# Copyright (c) 2022 zenobit from oSoWoSo
# mail: <zen@osowoso.xyz> web: https://osowoso.xyz
# licensed under EUPL 1.2
# source:
#

# Choose default shell
echo -e "\n${blue}Change user default shell...${none}\n"
printf "Possible (type in number): \n
- 1 Fish
- 2 Zsh\n"
read -p "Which shell do you want? (Enter for default bash)" shell
case $shell in
1 )
echo -e "\n${blue}Install Fish...${none}\n"
sudo xbps-install\
 fish-shell

sudo usermod --shell /bin/fish "$USER"
;;

2 )
echo -e "\n${blue}Install Zsh...${none}\n"
sudo xbps-install\
 zsh\
 zsh-autosuggestions\
 zsh-completions\
 zsh-history-substring-search\
 zsh-syntax-highlighting

sudo usermod --shell /bin/zsh "$USER"
;;

esac

echo done
