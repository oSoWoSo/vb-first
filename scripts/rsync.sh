#!/usr/bin/bash

# rsync mirror for vb aka VoidGNU/Linux
#
# Copyright (c) 2021 oSoWoSo <zen@osowoso.xyz>
# https://oSoWoSo.xyz
# licensed under EUPL 1.2
# sources:
#

rsync -av alpha.de.repo.voidlinux.org:xlocate /home/zen/Downloads/__aaa__void_repo_test
