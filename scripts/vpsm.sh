#!/usr/bin/bash

#
# vpsm install script for vb aka VoidGNU/Linux
#
# Copyright (c) 2022 zenobit from oSoWoSo
# mail: <zen@osowoso.xyz> web: https://osowoso.xyz
# licensed under EUPL 1.2
# source:
# 

sudo xbps-install vpsm
cd
git clone http://10.0.1.3:3000/oSoWoSo/VUR ~/.vur
cd .vur
alias -s XBPS_DISTDIR $HOME/.vur
vpsm bb
echo done
