#!/usr/bin/bash

#
# graphical apps install script for vb aka VoidGNU/Linux
#
# Copyright (c) 2022 zenobit from oSoWoSo
# mail: <zen@osowoso.xyz> web: https://osowoso.xyz
# licensed under EUPL 1.2
# source:
# 

sudo xbps-install\
 firefox\
 geany-plugins-extra\
 gimp\
 gparted\
 gpick\
 gscreenshot\
 meld\
 sakura\
 spacefm\
 vpnd\
 yad

echo done
