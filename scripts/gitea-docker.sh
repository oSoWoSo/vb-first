#!/usr/bin/bash

#
# gitea to docker install script for vb aka VoidGNU/Linux
#
# Copyright (c) 2022 zenobit from oSoWoSo
# mail: <zen@osowoso.xyz> web: https://osowoso.xyz
# licensed under EUPL 1.2
# source:
#

# program name
progname=${0##*/}
echo $progname

## COLORS
c0=$(tput setaf 0) # black
c1=$(tput setaf 1) # red
c2=$(tput setaf 2) # green
c3=$(tput setaf 3) # yellow
c4=$(tput setaf 4) # blue
c5=$(tput setaf 5) # 
c6=$(tput setaf 6) # light blue
c7=$(tput setaf 7) # white
c8=$(tput setaf 8) # grey
c9=$(tput setaf 9) # 
n=$(tput sgr0)

echo -n "${c2}Done${n}"
