# vb-scripts

[test](http://10.0.1.3:3000/oSoWoSo/vb-scripts/raw/branch/master/vb-linux_test-2022-01-29_21.41.46.mkv)

Void linux install script from LiveCD for virt-manager

* Adding cache drive to right place and install void linux by chroot or xbps..

Contain three independent script
1. `void-cd.sh` - Will install base system (run on live CD)
2. `void-chroot.sh` - Will configure installed system (run in chroot)
3. `void-final.sh` - Will install anything you choose (run in installed system)

* If you start from Live CD `void-chroot.sh` and `void-final.sh` will be downloaded automatically

# On Live CD

* Download script, make it executable and run it...

`curl https://20.0.0.100:3000/oSoWoSo/void-mini/raw/branch/master/void-cd.sh -o void-cd.sh`

* or

`wget https://20.0.0.100:3000/oSoWoSo/void-mini/raw/branch/master/void-cd.sh`

* or

just copy script to your text editor, save it, make it executable and run it

`chmod +x void-cd.sh`

- Must be run as super user or with sudo

`sudo ./void-cd.sh`

# In chroot 

`./void-chroot.sh`

# On installed system

`./void-final.sh`


And you are done!

## Enjoy your new void linux

# void-final.sh
Can be run alone on installed base system

Forked from void.sh

`https://codeberg.org/jhroot/void-sh.git`

void.sh is a script written in Bash to configure Void Linux.

Don't run as ROOT you will be asked for password if needed!

## Content of the `void-final.sh` script

* Update the system
* Install recommended packages
* Install development packages ***optional***
* Install non-free and multilib repository ***optional***
	* Install Nvidia proprietary drivers ***optional***
* Install another shell and make it the default ***optional***
* Install graphical user interface ***optional***
	* Choose a Desktop Environment ***optional***
	* Choose a Window Manager ***optional***
		* Choose a Display manager ***optional***
		* Install a terminal emulator ***optional***
		* Choose terminal text editor ***optional***
		* Choose gui text editor ***optional***
		* Choose an internet browser ***optional***
		* Choose a media player ***optional***
* Install some fonts ***optional***
* Install LibreOffice ***optional***
* Install GIMP + Inkscape ***optional***
* Install QEMU + Virt Manager ***optional***
* Choose a backup utility ***optional***
* Choose X keyboard language
* Enable required services
* Configure Cron
* Configure PulseAudio ***optional***
* Configure Network Management ***optional***
* Configure Bluetooth ***optional***
* Configure Printing support ***optional***
* Configure TLP for notebook power saving ***optional***
* Configure file sharing ***optional***
* Configure window manager  ***optional***
* Configure the Display manager ***optional***
* Start a Display manager ***optional***

## How to use it alone

* Check if you have installed the following packages:

`xbps-install git`

* Clone the repository:

`git clone https://codeberg.org/jhroot/void-sh.git`

* Execute the script **without root privileges**:

`cd void-sh` <br>
`sh void.sh`

## Notes

* Any script is not part of Void Linux, nor has it been developed by Void Linux developers.
* Use this script entirely at your own risk.

# void-cd.sh and void-chroot.sh licensed under EUPL
                      Copyright (c) 2021 oSoWoSo
                      Licensed under the EUPL-1.2

https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12

# void.sh have MIT License

Copyright (c) 2021 Raven
