#!/usr/bin/bash

#
# git push everywhere script for vb aka VoidGNU/Linux
#
# Copyright (c) 2022 zenobit from oSoWoSo
# mail: <zen@osowoso.xyz> web: https://osowoso.xyz
# licensed under EUPL 1.2
# source:
#

## ensure you are in git repo
#if [ ls grep .git ]; then
	#echo -e "${c2}***changme***${n}"
#else
	#echo -e "${c2}***changme***${n}"
	#sudo xbps-install ***changme***
#fi

REPO=$(pwd | rev |cut -d'/' -f 1 | rev)
# github
git remote add github git@github.com:oSoWoSo/"$REPO".git
git push github main
# codeberg
git remote add git@codeberg.org:oSoWoSo/"$REPO".git
git push codeberg main
echo pushed to codeberg
# pi
git remote add pi http://10.0.1.3:3000/oSoWoSo/"$REPO".git
git push pi main
echo "pushed to pi"
