#!/usr/bin/bash

#
# install script for void and vb linux
# intended to be runned from live cd
# (for use in virt-manager)
#
# Copyright (c) 2022 zenobit from oSoWoSo
# mail: <pm@osowoso.xyz> web: https://osowoso.xyz
# licensed under EUPL 1.2
# source:
# 

# Color definitions
green=$(tput setaf 2)
red=$(tput setaf 1)
none=$(tput sgr0)
# rootcheck
rootcheck () {
  if [[ $EUID -gt 0 ]]; then
    echo -e "\n${red}This operation needs super-user privileges.${none}\n"
    exit 255
  fi
}
rootcheck
echo -e "\n${green}Let's begin installing...${none}\n"
echo -e "\n${green}Change screen resolution to FullHD${none}\n"
xrandr -s 1920x1080
echo -e "\n${green}Mount xbps cache drive to right place${none}\n"
mount /dev/vdb1 /var/cache/
echo -e "\n${green}Changing mirrors to CZ one${none}\n"
mkdir -p /etc/xbps.d
cp /usr/share/xbps.d/*-repository-*.conf /etc/xbps.d/
sed -i 's|https://alpha.de.repo.voidlinux.org|https://mirror.fit.cvut.cz/voidlinux|g' /etc/xbps.d/*-repository-*.conf
echo -e "\n${green}After changing the URLs, you must synchronize xbps with the new mirrors:${none}\n"
xbps-install -Sy
echo -e "\n${green}Add some usefull packages${none}\n"
xbps-install -y xz
echo -e "\n${green}Creating partition must be added.. For now do it yourself${none}\n"
echo -e "\n${green}Expected two drives (one for boot partition and second root filesystem)${none}\n"
cfdisk
echo -e "\n${green}Format created drive${none}\n"
mkfs.vfat /dev/vda1
mkfs.btrfs /dev/vda2
echo -e "\n${green}mount created partition${none}\n"
mount /dev/vda2 /mnt/
mkdir -p /mnt/boot/
mount /dev/vda1 /mnt/boot/
echo -e "\n${green}Choose installation${none}\n"
printf "Possible (type in number): \n
- 1 rootfs
- 2 xbps\n"
read -p "rootfs or xbps...?" metod
case $metod in
	1 )
        echo -e "\n${green}download void linux ROOTFS tarball${none}\n"
        curl https://alpha.de.repo.voidlinux.org/live/current/void-x86_64-ROOTFS-20210930.tar.xz -o void-ROOTFS.tar.xz
        tar xvf void-ROOTFS.tar.xz -C /mnt
	;;

	2 )
        REPO=https://mirror.fit.cvut.cz/voidlinux/current
        ARCH=x86_64
        echo -e "\n${green}Install base system${none}\n"
        XBPS_ARCH=$ARCH xbps-install -Sy -r /mnt -R "$REPO" base-minimal ngetty glibc-locales bash gzip file sed \
                gawk less util-linux which tar xz man-pages mdocml shadow e2fsprogs btrfs-progs xfsprogs f2fs-tools \
                dosfstools procps-ng tzdata pciutils usbutils iana-etc openssh kbd iproute2 iputils iw xbps nvi sudo \
                void-artwork traceroute ethtool kmod acpid eudev runit-void removed-packages
        #XBPS_ARCH=$ARCH xbps-install -Sy -r /mnt -R "$REPO" base-system
        ;;
esac
echo -e "\n${green}Remount xbps drive under/mnt${none}\n"
cp -r /mnt/var/cache/ /var/cache/
umount /dev/vdb1
mount /dev/vdb1 /mnt/var/cache/
echo -e "\n${green}Entering chroot${none}\n"
mount --rbind /sys /mnt/sys && mount --make-rslave /mnt/sys
mount --rbind /dev /mnt/dev && mount --make-rslave /mnt/dev
mount --rbind /proc /mnt/proc && mount --make-rslave /mnt/proc
echo -e "\n${green}Copy the DNS configuration${none}\n"
cp /etc/resolv.conf /mnt/etc/
echo -e "\n${green}Copy setted mirrors${none}\n"
cp -r /etc/xbps.d /mnt/etc/
echo -e "\n${green}Copy installation script to chroot${none}\n"
curl http://10.0.2.3:3000/oSoWoSo/void-mini/raw/branch/master/void-chroot.sh -o void-chroot.sh
chmod +x void-chroot.sh
cp void-chroot.sh /mnt/
echo -e "\n${green}Chroot into the new installation${none}\n"
echo -e "\n${green}In chroot run ./void-chroot.sh${none}\n"
PS1='(chroot) # ' chroot /mnt/ /bin/bash
