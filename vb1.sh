#!/usr/bin/bash

#
# install script for vb aka VoidGNU/Linux
# intended to be runned from live cd
# (for use on real hardware)
# (Ryzen 5 2600 Nvidia 1050 Ti)
#
# Copyright (c) 2022 zenobit from oSoWoSo
# mail: <zen@osowoso.xyz> web: https://osowoso.xyz
# licensed under EUPL 1.2
# source:
# 

## COLORS
c0=$(tput setaf 0) # black
c1=$(tput setaf 1) # red
c2=$(tput setaf 2) # green
c3=$(tput setaf 3) # yellow
c4=$(tput setaf 4) # blue
c5=$(tput setaf 5) # 
c6=$(tput setaf 6) # light blue
c7=$(tput setaf 7) # white
c8=$(tput setaf 8) # grey
c9=$(tput setaf 9) # 
# reset color
n=$(tput sgr0)
# background
b0=$(tput setab 0)
b1=$(tput setab 1)
b2=$(tput setab 2)
b3=$(tput setab 3)
b4=$(tput setab 4)
b5=$(tput setab 5)
b6=$(tput setab 6)
b7=$(tput setab 7)
b8=$(tput setab 8)
b9=$(tput setab 9)
## rootcheck
check_root () {
	if [[ $EUID -gt 0 ]]; then
		echo -e "\n${c1}This operation needs super-user privileges."
		echo -e "${c2}exiting...${n}"
		exit 255
	fi
}
check_root
## Detect if this is an EFI system.
check_efi () {
	if [ -e /sys/firmware/efi/systab ]; then
		EFI_SYSTEM=1
		echo -e "\n${c2} EFI system enabled ${n}"
		EFI_FW_BITS=$(cat /sys/firmware/efi/fw_platform_size)
		if [ $EFI_FW_BITS -eq 32 ]; then
			EFI_TARGET=i386-efi
		else
			EFI_TARGET=x86_64-efi
		fi
	else
		echo -e "\n${c1} no EFI system support ${n}\n"
		echo -e "${c2}exiting...${n}"
		exit 1
	fi
}
check_efi

## begin
echo -e "\n${c2}-${c3}-${c1}- ${c2}Let's ${c3}begin ${c1}installing ${c2}-${c3}-${c1}-${n}"
## Unmount all partitions
unmount_all () {
	echo -e "\n${c4}Unmounting all partitions${c1}...${n}"
	umount /dev/sda1
	umount /dev/sda2
	umount /dev/sda3
	umount /dev/sda4
	umount /dev/sdb1
	umount /dev/sdb2
	umount /dev/sdb3
	umount /dev/sdb4
	umount /dev/vda1
	umount /dev/vda2
	umount /dev/vda3
	umount /dev/vda4
	umount /dev/vdb1
	umount /dev/vdb2
	umount /dev/vdb3
	umount /dev/vdb4
	umount /dev/nvme0n1p1
	umount /dev/nvme0n1p2
	umount /dev/nvme0n1p3
	umount /dev/nvme0n1p4
}
unmount_all

echo -e "${c4}Mounting cache${c1}...${n}"
mount /dev/nvme0n1p4 /var/cache/

echo -e "${c2}Changing mirrors...${n}"
defaultrepo="https://alpha.de.repo.voidlinux.org"
echo "default repository: ${defaultrepo}"
newrepo="https://mirror.fit.cvut.cz/voidlinux"
echo "new repository: ${newrepo}"
mkdir -p /etc/xbps.d
cp /usr/share/xbps.d/*-repository-*.conf /etc/xbps.d/
sed -i "s|$defaultrepo|$newrepo|g" /etc/xbps.d/*-repository-*.conf
echo -e "${c4}Synchronizing${c1}...${n}"
xbps-install -Suvy

echo -e "\n${c1}Required${n} two partitions (${c1}boot${n} and ${c1}root${n})"
echo -e "- ${c3}recommended${n} separate home partition"
echo -e "- ${c3}optional${n} separate cache partition"

use_gparted () {
	echo -e "${c2}Use gparted...?${n}" && read gpart
	case $gpart in
		y )
			gparted
		;;
		0 )
		;;
	esac
}
use_gparted

echo -e "${c1}Drives list:${n}"
fdisk -x | grep "/dev/"

set_boot () {
	echo -e "${c2}Enter partition for boot: ${c1}required${n}"
	echo -e "(${c4}blue${n} = personal defaults)"
	echo -e "(sda1 ${c4}sdb1${n} vda1 nvme0n1p1${c1}...${n})"
	read -p "" boot
	echo -e "${c4}root will be on ${c1}${boot}${n}"
	export boot
}
set_boot

set_root () {
	echo -e "\n${c2}Enter partition for root: ${c1}required${n}"
	echo -e "(sda2 ${c4}sdb2${n} vda2 nvme0n1p2${c1}...${n})"
	read -p "" root
	echo -e "${c4}root will be on ${c1}${root}${n}"
	export root
}
set_root

set_home () {
	echo -e "\n${c2}Enter partition for home: ${c3}recommended${n}"
	echo -e "(sda3 sdb3 vda3 ${c4}nvme0n1p3${n}${c1}...${n})"
	read -p "" home
	echo -e "${c4}root will be on ${c1}${home}${n}"
	export home
}
set_home

set_cache () {
	echo -e "\n${c2}Enter partition for cache: ${c3}optional${n}"
	echo -e "(sda4 sdb4 vda4 ${c4}nvme0n1p4${n}${c1}...${n})"
	read -p "" cache
	echo -e "${c4}root will be on ${c1}${cache}${n}"
	export cache
}
set_cache

format () {
	echo -e "\n${c4}Formating partitions${c1}...${n}"
	mkfs.vfat /dev/${boot}
	echo -e "${c2}done${n}"
	mkfs.btrfs -f /dev/${root}
	echo -e "${c2}done${n}"
}
format

mount_target () {
	echo -e "${c4}Mounting partitions${c1}...${n}"
	mkdir -p /mnt/target
	mount /dev/${root} /mnt/target/
	mkdir -p /mnt/target/boot
	mount /dev/${boot} /mnt/target/boot
	mkdir -p /mnt/target/home
	mount /dev/${home} /mnt/target/home/
	mkdir -p /mnt/target/var/cache
	mount /dev/${cache} /mnt/target/var/cache/
}
mount_target

install_method () {
	echo -e "\n${c2}Choose installation method...${n}"
	echo -e "Possible (type in number):
	- 1 rootfs
	- 2 xbps ${c3}recommended${n}
	- 3 cd copy"
	read -p "" method
	case $method in
		1 )
			xbps-install -y xz
			echo -e "${c4}Downloading void linux ROOTFS tarball${c1}...${n}"
			## Detect if ROOTFS already downloaded.
			if [ -e ./void-ROOTFS.tar.xz ]; then
				tar xvf void-ROOTFS.tar.xz -C /mnt/target
			else
				curl https://mirror.fit.cvut.cz/voidlinux/live/current/void-x86_64-ROOTFS-20210930.tar.xz -o void-ROOTFS.tar.xz
				tar xvf void-ROOTFS.tar.xz -C /mnt/target
			fi
		;;
	
		2 )
			REPO=${newrepo}/current
			ARCH=x86_64
			echo -e "${c4}Installing base system${c1}...${n}"
			XBPS_ARCH=$ARCH xbps-install -Sy -r /mnt/target -R "$REPO" base-system fish-shell refind
			;;
		3 )
			echo "Not implemented yet!"
		;;
	esac
}
install_method

echo -e "\n${c2}Entering chroot${n}"
echo -e "${c4}Remounting cache${c1}...${n}"
cp -r /mnt/target/var/cache/ /var/
umount /dev/${cache}
mount /dev/${cache} /mnt/target/var/cache/

mount_chroot () {
	mount --rbind /sys /mnt/target/sys && mount --make-rslave /mnt/target/sys
	mount --rbind /dev /mnt/target/dev && mount --make-rslave /mnt/target/dev
	mount --rbind /proc /mnt/target/proc && mount --make-rslave /mnt/target/proc
}
mount_chroot

echo -e "${c4}Copying the DNS configuration${c1}...${n}"
cp /etc/resolv.conf /mnt/target/etc/

echo -e "${c4}Copying setted mirrors${c1}...${n}"
cp -r /etc/xbps.d /mnt/target/etc/

echo -e "${c4}Copying installation script to chroot${c1}...${n}"
curl http://10.0.1.3:3000/oSoWoSo/vb-scripts/raw/branch/master/vb2.sh -o vb2.sh
chmod +x vb2.sh
cp vb2.sh /mnt/target

echo -e "\n${c2}Chroot into the new installation${n}"
PS1='(chroot) # ' chroot /mnt/target/ /bin/bash  "./vb2.sh"
